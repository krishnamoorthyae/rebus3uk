package uk.co.three.rebus.three_rebus.core.productmodel.service;

import uk.co.three.rebus.three_rebus.core.productmodel.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.ProductDataGetResponse;
/**
 * This is the interface for performing actions
 * on the offering details
 * @author DD00493288
 *
 */
public interface OfferingsDetailsProvider {

	public ProductDataGetResponse getOfferingsDetails(ProductDataGetRequest productDataGetRequest);

}
