/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.servlets;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author DD00493288
 *
 */
public class TestParallelThreads {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestParallelThreads inst = new TestParallelThreads();
		inst.testParallelThread();
	}
	
	/**
	 * 
	 */
	private void testParallelThread(){
		final int THREAD_SIZE = 10000;
		List<Future<TestModel>> futures = new ArrayList<Future<TestModel>>();
		ExecutorService executorService = Executors.newFixedThreadPool(THREAD_SIZE);
		List<TestThread> threadList = new ArrayList<TestThread>();
		for(int i=0;i<THREAD_SIZE;i++) {
			TestModel model = new TestModel();
			TestThread testThread = new TestThread(model, i);
			threadList.add(testThread);
			Future<TestModel> future = executorService.submit(
					testThread, model);
			futures.add(future);
		}
		executorService.shutdown();
		boolean finished = false;
		
		try {
			while (!(finished = executorService.awaitTermination(
					1000,
					TimeUnit.SECONDS))) {
			}
		} catch (InterruptedException e) {
			System.out.println(e.getLocalizedMessage());
		
		}
		if (executorService.isTerminated()) {
			long max =0;
			long min=0;
			int forCounter=0;
			for (Future<TestModel> futureInst : futures) {
				try {
					TestModel testModel = futureInst.get();
					if(forCounter == 0){
						max = testModel.getTotalExecTime();
						min = testModel.getTotalExecTime();
						forCounter++;
						continue;
					}
					
					long totalExecTime = testModel.getTotalExecTime();
					if(totalExecTime < min ){
						min = totalExecTime;
					}
					
					if(totalExecTime > max){
						max = totalExecTime;
					}
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			System.out.println("Min="+min+";Max="+max);
		
		}
		
		
		
	}

}
