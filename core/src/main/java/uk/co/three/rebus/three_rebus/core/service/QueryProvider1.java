/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.service;

import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.InvalidQueryException;

import epc.h3g.com.ProductOfferingSpecification;

/**
 * @author AK00478684
 *
 */
public interface QueryProvider1 {
	public List<ProductOfferingSpecification> getProductOfferSample(Session session, String productName)
			throws InvalidQueryException, RepositoryException;
}
