package uk.co.three.rebus.three_rebus.core.productmodel.service;

import uk.co.three.rebus.three_rebus.core.productmodel.response.ProductDataGetResponse;
/**
 * 
 * @author DD00493288
 *
 */
public interface ProductImporterService {
	public void doImport();
	public ProductDataGetResponse getProductDataResponse();

}
