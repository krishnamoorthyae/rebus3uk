package uk.co.three.rebus.three_rebus.core.productmodel.service;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;

public interface SessionHandlerService {

	public ResourceResolver getResourceResolver() throws LoginException;

	public Session openSession() throws LoginException;
	
	public Session getSession() throws javax.jcr.LoginException, RepositoryException;

}
