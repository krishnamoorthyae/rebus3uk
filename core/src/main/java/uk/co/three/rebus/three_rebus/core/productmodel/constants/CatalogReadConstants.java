/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.constants;

/**
 * This is constants class for the read functionality in catalog module
 * @author DD00493288
 *
 */
public class CatalogReadConstants {

	public static final String PRODUCT_DETAIL_PATH = "ProductPath";
	
}
