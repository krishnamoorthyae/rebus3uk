package uk.co.three.rebus.three_rebus.core.productmodel.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.productmodel.constants.CatalogWriteConstants;
import uk.co.three.rebus.three_rebus.core.productmodel.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.request.ProductOfferRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.productmodel.service.ProductImporterService;
import uk.co.three.rebus.three_rebus.core.productmodel.write.CatalogInfomationWriter;
import uk.co.three.rebus.three_rebus.core.session.SessionHandlerImpl;
import uk.co.three.rebus.three_rebus.core.util.CommonUtils;

/**
 * This class is responsible for providing utilite's to handle product catalog
 * in repository .
 * 
 * @author MS00506946
 *
 */

@Component
@Service(value = { ProductImporterService.class })
public class ThreeProductImporter extends SessionHandlerImpl implements ProductImporterService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	private OfferingsDetailsProvider offeringsDetailsProvider;

	protected String ABSOLUTE_PATH = "/content/threeRebus/products";
	// 7/6
	protected String STORE_NAME = "ThreeStore";
	protected String CATALOGUE_NAME = "CatalogOne";
	private Map<Integer, String> errorMap = new HashMap<Integer, String>();

	// 8/6

	@Override
	@Activate
	public void doImport() {
		getProductDataResponse();
	}

	@Override
	// @Activate
	public ProductDataGetResponse getProductDataResponse() {
		Session session = null;
		String productPath = null;
		ProductDataGetResponse productDataGetResponse = new ProductDataGetResponse();
		try {
			/**
			 * Code commented to fix closed session issue
			 */
			/*
			 * ResourceResolver resourceResolver = getResourceResolver();
			 * session = openSession();
			 */
			/**
			 * New code to get the session
			 */
			session = getSession();
			Node storeRoot = session.getNode(ABSOLUTE_PATH);

			/**
			 * Code commented to fix closed session issue
			 */
			/*
			 * Resource resource = resourceResolver.getResource(ABSOLUTE_PATH);
			 */
			/* Node storeRoot = resource.adaptTo(Node.class); */
			Node catalogNode;
			Node storeNode;
			if (!storeRoot.hasNode(STORE_NAME)) {
				storeNode = addStoreName(session, ABSOLUTE_PATH);
				// String storePath=storeNode.getPath();

			} else {
				// 7/6
				storeNode = storeRoot.getNode(STORE_NAME);
			}
			Node versionNode = createVersionNode(storeNode, session);
			catalogNode = versionNode.addNode(CATALOGUE_NAME);
			catalogNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.CATALOG);
			productPath = versionNode.getPath() + "/" + CATALOGUE_NAME;
			addProduct(session, productPath);

			CommonUtils.setErrorMessages(productDataGetResponse, errorMap);
		} catch (LoginException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.LOGIN_EXEPTION,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.LOGIN_EXEPTION_MESSAGE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
		return productDataGetResponse;
	}

	/**
	 * This method will add store name into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,absolutePath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param absolutePath
	 *            shows exact location where operation need to perform .
	 * 
	 * @return void
	 * 
	 */

	private Node addStoreName(Session session, String absolutePath) {
		Node storeNode = null;
		try {
			Node productsNode = session.getNode(absolutePath);
			storeNode = productsNode.addNode(STORE_NAME);
			// 7/6
			storeNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.STORE);

		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
		return storeNode;
	}

	/**
	 * This method will add versionNode under store node path
	 * 
	 * Passing session ,storeNode
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param storeNode
	 *            shows the exact location where version node need to be created
	 *            .
	 * 
	 * @return Node object i.e. versionNode
	 * 
	 */
	private Node createVersionNode(Node storeNode, Session session) {
		Node versionNode = null;
		try {
			NodeIterator versionNodeList = storeNode.getNodes();
			versionNodeList.getSize();
			long version = 0;
			while (versionNodeList.hasNext()) {
				versionNode = versionNodeList.nextNode();
				version = versionNode.getProperty(CatalogWriteConstants.VERSION).getLong();
			}
			int versionNumber = (int) version;
			int latestVersion = versionNumber + 1;
			String versionName = "Version" + latestVersion;
			versionNode = storeNode.addNode(versionName);
			versionNode.setProperty(CatalogWriteConstants.VERSION, latestVersion);
			versionNode.setProperty(CatalogWriteConstants.NAME, versionName);
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
		return versionNode;

	}

	/**
	 * This method will add product into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,productPath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param productPath
	 *            shows exact location where product need to be stored .
	 * 
	 * @return void
	 * 
	 */

	private void addProduct(Session session, String productPath) {
		try {
			CatalogInfomationWriter catalogInfomationWriter = new CatalogInfomationWriter();
			Node catalogNode = session.getNode(productPath);
			ProductDataGetRequest productDataGetRequest = new ProductDataGetRequest();
			productDataGetRequest.setSession(session);
			ProductDataGetResponse productDataGetResponse = offeringsDetailsProvider
					.getOfferingsDetails(productDataGetRequest);
			ProductOfferRequest productOfferRequest = new ProductOfferRequest();
			if (!productDataGetResponse.hasErrors()) {
				productOfferRequest.setCatalogNode(catalogNode);
				productOfferRequest.setProductDataGetResponse(productDataGetResponse);
				productOfferRequest.setSession(session);
				productDataGetResponse = catalogInfomationWriter.offerDetailsProcessor(productOfferRequest);
				session.save();
			}
		} catch (PathNotFoundException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.PATHNOTFOUND_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.PATHNOTFOUND_FAILURE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
	}
}
