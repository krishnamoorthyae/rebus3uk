package uk.co.three.rebus.three_rebus.core.session;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import uk.co.three.rebus.three_rebus.core.productmodel.service.SessionHandlerService;

@Component
@Service(value = { SessionHandlerService.class })

/**
 * This class is responsible for handling the session
 * 
 * @author 
 *
 */

public class SessionHandlerImpl implements SessionHandlerService {

	/**
	 * static instance of this class
	 */
	private static SessionHandlerImpl s_instance = null;
	
	@Reference
	private ResourceResolverFactory resourceFactory;

	@Reference
	private SlingRepository slingRepository;
	
	/**
	 * private constructor
	 *//*
	private SessionHandlerImpl(){
		// do nothing as of now
	}
	
	*//**
	 * Public method to get this instance
	 * @return
	 *//*
	public static SessionHandlerImpl getInstance(){
		if(s_instance == null){
			s_instance = new SessionHandlerImpl();
		}
		return s_instance;
	}*/
	
	
	/* * This method is used to provide the ResourceResolver objects to perform operations.
	 * 
	 * @return ResourceResolver
	 * @exception LoginException
	 * 
	 */
	@Override
	public ResourceResolver getResourceResolver() throws LoginException {
		Map<String, Object> productImp = new HashMap<String, Object>();

		productImp.put(ResourceResolverFactory.SUBSERVICE, "readService");

		ResourceResolver resourceResolver = resourceFactory.getServiceResourceResolver(productImp);

		return resourceResolver;
	}

	/**
	 * This method is used to provide the session objects to perform Transactions.
	 * 
	 * @return session
	 * @exception LoginException
	 * 
	 */
	
	@Override
	public Session openSession() throws LoginException {
		Session session = null;
		ResourceResolver resourceResolver = getResourceResolver();
		session = resourceResolver.adaptTo(Session.class);

		return session;
	}
	
	/**
	 * 
	 * @return
	 * @throws RepositoryException 
	 * @throws javax.jcr.LoginException 
	 */
	public Session getSession() throws javax.jcr.LoginException, RepositoryException {
		final String serviceName = "readService";
		final String workspaceName = null;
		return slingRepository.loginService(serviceName, workspaceName);
	}

}
