/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.servlets;

/**
 * @author DD00493288
 *
 */
public class TestModel {

	private long startTime = 0;
	
	private long endTime = 0;

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public long getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getTotalExecTime(){
		return endTime - startTime;
	}
	
}
