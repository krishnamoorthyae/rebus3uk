package uk.co.three.rebus.three_rebus.core.productmodel.write;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.AccountingItemSpecification;
import epc.h3g.com.AccountingSchemeSpecification;
import epc.h3g.com.AddressLocalization;
import epc.h3g.com.AllowanceGroupRelationshipSpecification;
import epc.h3g.com.AllowanceGroupSpecification;
import epc.h3g.com.AllowancePeriodicity;
import epc.h3g.com.AllowanceSpecification;
import epc.h3g.com.ApplicabilityRule;
import epc.h3g.com.ApplicabilitySpecification;
import epc.h3g.com.AvailabilityRule;
import epc.h3g.com.BillingCycleSpecification;
import epc.h3g.com.Characteristic;
import epc.h3g.com.CharacteristicGroupSpecification;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CharacteristicValue;
import epc.h3g.com.ChargeSpecification;
import epc.h3g.com.ChargeType;
import epc.h3g.com.CommissionSpecification;
import epc.h3g.com.CompatibilityRule;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.CustomerAccountType;
import epc.h3g.com.CustomerSegment;
import epc.h3g.com.DiscountSpecification;
import epc.h3g.com.EligibilityRule;
import epc.h3g.com.GenericRelationshipSpecification;
import epc.h3g.com.Localization;
import epc.h3g.com.Location;
import epc.h3g.com.MarketSegment;
import epc.h3g.com.Money;
import epc.h3g.com.PartyPricePlanSpecification;
import epc.h3g.com.PaymentMethod;
import epc.h3g.com.PeriodRule;
import epc.h3g.com.PriceItemSpecification;
import epc.h3g.com.PricePlanSpecification;
import epc.h3g.com.ProductOfferingRelationshipSpecification;
import epc.h3g.com.ProductOfferingSpecification;
import epc.h3g.com.ProductSpecification;
import epc.h3g.com.RelationshipInclusivityType;
import epc.h3g.com.RelationshipSpecification;
import epc.h3g.com.RolloverSpecification;
import epc.h3g.com.SecurityRule;
import epc.h3g.com.SellingChannel;
import epc.h3g.com.Specification;
import epc.h3g.com.SpendingLimitRule;
import epc.h3g.com.SpendingLimitSpecification;
import epc.h3g.com.SpendingUnit;
import epc.h3g.com.StatusHistory;
import epc.h3g.com.SupplierPartner;
import epc.h3g.com.ValueExpression;
import epc.h3g.com.ValueExpressionItem;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.productmodel.constants.CatalogWriteConstants;
import uk.co.three.rebus.three_rebus.core.productmodel.model.PathModel;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

/**
 * This class is responsible for handling Specification data And inserting the
 * data into repository . Its super class of CatalogInfomationWriter. And Sub
 * class of CharacteristicWriter
 * 
 * @author MS00506946
 *
 */
public class SpecificationWriter extends CharacteristicWriter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * This method is used to add the nodes and properties which are related to
	 * pricePlanSpecification
	 * 
	 * Passing productOfferNode ,pricePlanSpecificationList and pathDataList
	 * 
	 * @param productOfferNode
	 *            is used to get the PricePlanSpecification object to
	 *            productOfferNode object .
	 * @param pricePlanSpecificationList
	 *            is used to iterate the List of PricePlanSpecification
	 * @param pathDataList
	 *            is used to hold the exact location of related specification
	 *            node where we need to set the absolute path of related
	 *            specification .
	 * 
	 * @return void
	 * 
	 */

	protected void pricePlanSpecification(Node productOfferNode,
			List<PricePlanSpecification> pricePlanSpecificationList, List<PathModel> pathDataList) {
		if (!pricePlanSpecificationList.isEmpty()) {
			try {
				Node pricePlansNode = productOfferNode.addNode(CatalogWriteConstants.PRICE_PLANS_NODE);
				pricePlansNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.PRICE_PLANS);
				for (PricePlanSpecification pricePlanSpecification : pricePlanSpecificationList) {
					Node pricePlanSpecificationNode = pricePlansNode.addNode(pricePlanSpecification.getName(),
							CatalogWriteConstants.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(pricePlanSpecificationNode, pricePlanSpecification);
					specificationSetProperty(pricePlanSpecificationNode, pricePlanSpecification, pathDataList);
					pricePlanSpecificationNode.setProperty(CatalogWriteConstants.STATUS,
							pricePlanSpecification.getStatus().name());
					pricePlanSpecificationNode.setProperty(CatalogWriteConstants.STORE_INVENTORY,
							pricePlanSpecification.isStoreInventory());
					List<PriceItemSpecification> priceItemSpecList = pricePlanSpecification.getItem();
					priceItemSpec(pricePlanSpecificationNode, priceItemSpecList);
					List<Characteristic> characteristicList = pricePlanSpecification.getCharacteristic();
					if (!characteristicList.isEmpty()) {
						characteristicList.forEach(characteristic -> {
							try {
								Node characteristicNode = pricePlanSpecificationNode.addNode(characteristic.getName(),
										CatalogWriteConstants.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(characteristicNode, characteristic);
								// 19/6
								CharacteristicSpecification characteristicSpecification = characteristic
										.getSpecification();
								if (characteristicSpecification != null) {
									characteristicSpecification(characteristicNode, characteristicSpecification);
								}
								CharacteristicValue characteristicValue = characteristic.getValue();
								if (characteristicValue != null) {
									createCharacteristicValueNode(characteristicNode, characteristicValue,
											CatalogWriteConstants.CHARACTERISTIC_VALUE);
								}
							} catch (Exception e) {
								log.info("Error:" + e.getLocalizedMessage());
								errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE, FetchErrorMessage
										.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
							}
						});
					}
					List<AllowanceGroupSpecification> allowanceGroupSpecification = pricePlanSpecification
							.getAllowanceGroup();
					if (!allowanceGroupSpecification.isEmpty()) {
						for (AllowanceGroupSpecification allowanceGroupSpecificationObj : allowanceGroupSpecification) {
							createAllowancegroupSpecNode(pricePlanSpecificationNode, allowanceGroupSpecificationObj);
						}
					}
					PeriodRule periodRule = pricePlanSpecification.getPeriodRule();
					if (periodRule != null) {
						Node periodRuleNode = pricePlanSpecificationNode.addNode(periodRule.getName(),
								CatalogWriteConstants.NT_UNSTRUCTURED);
						periodRuleNode.setProperty(CatalogWriteConstants.START_OFFSET_UNITS,
								periodRule.getStartOffsetUnits());
						// if (periodRule.getEndOffsetUnits() != 0)
						periodRuleNode.setProperty(CatalogWriteConstants.END_OFFSET_UNITS,
								periodRule.getEndOffsetUnits() != null ? periodRule.getEndOffsetUnits() : 0);
						if (periodRule.getEndOffsetUnitType() != null) {
							periodRuleNode.setProperty(CatalogWriteConstants.UNIT_TYPE,
									periodRule.getEndOffsetUnitType().value());
						}
						if (periodRule.getStartOffsetUnitType() != null) {
							periodRuleNode.setProperty(CatalogWriteConstants.UNIT_TYPE,
									periodRule.getStartOffsetUnitType().value());
						}
						if (periodRule.getEndMark() != null) {
							Node endMarkNode = periodRuleNode.addNode(periodRule.getEndMark().getName(),
									CatalogWriteConstants.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(endMarkNode, periodRule.getEndMark());
						}
						if (periodRule.getStartMark() != null) {
							Node startMarkNode = periodRuleNode.addNode(periodRule.getStartMark().getName(),
									CatalogWriteConstants.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(startMarkNode, periodRule.getStartMark());
						}
					}
					createEligibilityRuleNode(pricePlanSpecificationNode, pricePlanSpecification);
					
					if (pricePlanSpecification instanceof PartyPricePlanSpecification) {
						List<ProductOfferingSpecification> productOffering = ((PartyPricePlanSpecification) pricePlanSpecification)
								.getProductOffering();
						if (!productOffering.isEmpty()) {
							for (ProductOfferingSpecification prodOfferingSpec : productOffering) {
								// productOfferingSpec(prodOfferingSpec,
								// pricePlanSpecificationNode, pathDataList);
							}
						}
					}
				}
			} catch (RepositoryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * Specification
	 * 
	 * Passing node ,specification
	 * 
	 * @param node
	 *            is used to set the property
	 * @param specification
	 *            is used to hold the Specification Data
	 * @param pathDataList
	 *            is used to hold the exact location of related specification
	 *            node where we need to set the absolute path of related
	 *            specification .
	 * 
	 * @return void
	 * 
	 */

	protected void specificationSetProperty(Node node, Specification specification, List<PathModel> pathDataList) {
		try {

			List<CharacteristicGroupSpecification> characteristicGroupSpecificationList = specification
					.getCharacteristicGroupSpecification();
			if (!characteristicGroupSpecificationList.isEmpty()) {
				Node charGroupSpecsNode = node.addNode(CatalogWriteConstants.CHARACTERISTIC_GROUP_SPECS_NODE);
				charGroupSpecsNode.setProperty(CatalogWriteConstants.TYPE,
						CatalogWriteConstants.CHARACTERISTIC_GROUP_SPECIFICATIONS);
				for (CharacteristicGroupSpecification characteristicGroupSpecification : characteristicGroupSpecificationList) {
					createCharacteristicGroupSpecificationNode(charGroupSpecsNode, characteristicGroupSpecification);
				}
			}
			List<CharacteristicSpecification> characteristicSpecificationList = specification
					.getCharacteristicSpecification();
			if (!characteristicSpecificationList.isEmpty()) {
				// 31/05
				Node charSpecsNode = node.addNode(CatalogWriteConstants.CHARACTERISTIC_SPECIFICATIONS_NODE);
				charSpecsNode.setProperty(CatalogWriteConstants.TYPE,
						CatalogWriteConstants.CHARACTERISTIC_SPECIFICATIONS);
				for (CharacteristicSpecification charSpec : characteristicSpecificationList) {
					characteristicSpecification(charSpecsNode, charSpec);
				}

			}
			List<StatusHistory> statusHistoryList = specification.getHistory();
			if (!statusHistoryList.isEmpty()) {
				for (StatusHistory statusHistoryObj : statusHistoryList) {
					// 8/6
					statusHistory(node, statusHistoryObj);
				}
			}

			List<RelationshipSpecification> ReferencedSpecificationList = specification.getReferencedSpecification();
			if (!ReferencedSpecificationList.isEmpty()) {
				Node referencedSpecsNode = node.addNode(CatalogWriteConstants.REFERENCED_SPECIFICATIONS,
						CatalogWriteConstants.NT_UNSTRUCTURED);
				for (RelationshipSpecification referencedSpecificationObj : ReferencedSpecificationList) {
					// 14/6
					if (referencedSpecificationObj instanceof ProductOfferingRelationshipSpecification) {
						ProductOfferingRelationshipSpecification prodOfferRelationshipSpec = (ProductOfferingRelationshipSpecification) referencedSpecificationObj;
						Node referencedSpecificationNode = referencedSpecsNode
								.addNode(prodOfferRelationshipSpec.getName());
						entitySpecificationSetProperty(referencedSpecificationNode, prodOfferRelationshipSpec);
						relationshipSpecification(referencedSpecificationNode, prodOfferRelationshipSpec, pathDataList);
						if (prodOfferRelationshipSpec.isCustomerAssetReuse() != null) {
							referencedSpecificationNode.setProperty(CatalogWriteConstants.CUSTOMER_ASSET_REUSE,
									prodOfferRelationshipSpec.isCustomerAssetReuse());
						}
						if (prodOfferRelationshipSpec.isIsAccountOwner() != null) {
							referencedSpecificationNode.setProperty(CatalogWriteConstants.IS_ACCOUNT_OWNER,
									prodOfferRelationshipSpec.isIsAccountOwner());
						}
						RelationshipInclusivityType inclusivityType = prodOfferRelationshipSpec.getInclusivityType();
						if (inclusivityType != null) {
							referencedSpecificationNode.setProperty(CatalogWriteConstants.INCLUSIVE_TYPE,
									inclusivityType.value());
						}
						List<SecurityRule> securityRuleList = prodOfferRelationshipSpec.getSecurityRule();
						if (!securityRuleList.isEmpty()) {
							for (SecurityRule securityRule : securityRuleList) {
								Node securityRuleNode = referencedSpecificationNode.addNode(securityRule.getName());
								entitySpecificationSetProperty(securityRuleNode, securityRule);
								securityRuleNode.setProperty(CatalogWriteConstants.CASCADE_DOWN,
										securityRule.isCascadeDown());
								securityRuleNode.setProperty(CatalogWriteConstants.FULL_RIGHTS,
										securityRule.isFullRights());
								ConditionExpression conditionExpression = securityRule.getConditionExpression();
								if (conditionExpression != null) {
									createConditionExpressionNode(securityRuleNode, conditionExpression);
								}
								List<AllowanceGroupSpecification> allowanceGroupAccessList = securityRule
										.getAllowanceGroupAccess();
								if (!allowanceGroupAccessList.isEmpty()) {
									for (AllowanceGroupSpecification allowanceGroupAccess : allowanceGroupAccessList) {
										createAllowancegroupSpecNode(securityRuleNode, allowanceGroupAccess);
									}
								}
								List<PriceItemSpecification> priceItemAccessList = securityRule.getPriceItemAccess();
								if (!priceItemAccessList.isEmpty()) {
									priceItemSpec(securityRuleNode, priceItemAccessList);
								}
							}
						}
					} else if (referencedSpecificationObj instanceof GenericRelationshipSpecification) {
						GenericRelationshipSpecification genericRelationshipSpec = (GenericRelationshipSpecification) referencedSpecificationObj;
						Node referencedSpecificationNode = referencedSpecsNode
								.addNode(referencedSpecificationObj.getName());
						referencedSpecificationNode.setProperty(CatalogWriteConstants.ROLE,
								genericRelationshipSpec.getRole() != null ? genericRelationshipSpec.getRole() : null);
						entitySpecificationSetProperty(referencedSpecificationNode, genericRelationshipSpec);
						relationshipSpecification(referencedSpecificationNode, genericRelationshipSpec, pathDataList);
					}
				}
			}
			node.setProperty(CatalogWriteConstants.STORE_INVENTORY, specification.isStoreInventory());
			if (specification.getStatus() != null) {
				// Node statusNode =
				// node.addNode(specification.getStatus().value());
				node.setProperty(CatalogWriteConstants.STATUS, specification.getStatus().value());
			}

		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * priceItemSpec .
	 * 
	 * Passing pricePlanNode ,priceItemSpec
	 * 
	 * @param pricePlanNode
	 *            is used to add the property .
	 * @param priceItemSpec
	 *            object is used to iterate the List of PriceItemSpecification .
	 * 
	 * @return void
	 * 
	 */

	private void priceItemSpec(Node pricePlanNode, List<PriceItemSpecification> priceItemSpec) {
		try {
			if (!priceItemSpec.isEmpty()) {
				// 23/6
				Node priceItemsNode = pricePlanNode.addNode(CatalogWriteConstants.PRICE_ITEMS_NODE);
				priceItemsNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.PRICE_ITEMS);
				for (PriceItemSpecification priceItemSpecification : priceItemSpec) {
					Node priceItemSpecificationNode = priceItemsNode.addNode(priceItemSpecification.getName());
					entitySpecificationSetProperty(priceItemSpecificationNode, priceItemSpecification);
					priceItemSpecificationNode.setProperty(CatalogWriteConstants.MIN_MULTIPICITY,
							priceItemSpecification.getMinMultiplicity().longValue());
					// 14/6
					if (priceItemSpecification.getMaxMultiplicity() != null) {
						priceItemSpecificationNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
								priceItemSpecification.getMaxMultiplicity().longValue());
					}
					if (priceItemSpecification.isOverrideChildren() != null) {
						priceItemSpecificationNode.setProperty(CatalogWriteConstants.OVERRIDE_CHILDREN,
								priceItemSpecification.isOverrideChildren());
					}
					if (priceItemSpecification.isUseForContractRenew() != null)
						priceItemSpecificationNode.setProperty(CatalogWriteConstants.IS_USABLE_FOR_CONTRACT_RENEW,
								priceItemSpecification.isUseForContractRenew());

					ApplicabilitySpecification applicability = priceItemSpecification.getApplicability();
					if (applicability != null) {
						applicabilitySpecNode(priceItemSpecificationNode, applicability);
					}

					if (priceItemSpecification.getSingleViewDetails() != null) {
						Node singleViewDetailsNode = priceItemSpecificationNode.addNode(
								CatalogWriteConstants.SINGLE_VIEW_DETAILS, CatalogWriteConstants.NT_UNSTRUCTURED);
						List<AccountingSchemeSpecification> accountingSchemeSpecificationList = priceItemSpecification
								.getSingleViewDetails().getAccountingScheme();
						if (!accountingSchemeSpecificationList.isEmpty()) {
							for (AccountingSchemeSpecification accountingSchemeSpecification : accountingSchemeSpecificationList) {
								Node accountingSchemeSpecificationNode = singleViewDetailsNode
										.addNode(accountingSchemeSpecification.getName());
								entitySpecificationSetProperty(accountingSchemeSpecificationNode,
										accountingSchemeSpecification);
								List<AccountingItemSpecification> accountingItemSpecificationList = accountingSchemeSpecification
										.getItem();
								if (!accountingItemSpecificationList.isEmpty()) {
									for (AccountingItemSpecification accountingItemSpecification : accountingItemSpecificationList) {
										Node accountingItemSpecificationNode = accountingSchemeSpecificationNode
												.addNode(accountingItemSpecification.getName());
										entitySpecificationSetProperty(accountingItemSpecificationNode,
												accountingItemSpecification);
										if (accountingItemSpecification.getCreditAccount() != null)
											accountingItemSpecificationNode.setProperty(
													CatalogWriteConstants.CREDIT_AMOUNT,
													accountingItemSpecification.getCreditAccount());
										if (accountingItemSpecification.getDebitAccount() != null)
											accountingItemSpecificationNode.setProperty(
													CatalogWriteConstants.DEBIT_AMOUNT,
													accountingItemSpecification.getDebitAccount());
										accountingItemSpecificationNode.setProperty(
												CatalogWriteConstants.AMOUNT_PERCENTAGE,
												accountingItemSpecification.getAmountPercentage() != null
														? accountingItemSpecification.getAmountPercentage().intValue()
														: 0);
										if (accountingItemSpecification.getFixedAmount() != null) {
											Node fixedAmountNode = accountingItemSpecificationNode.addNode(
													CatalogWriteConstants.FIXED_AMOUNT,
													CatalogWriteConstants.NT_UNSTRUCTURED);
											fixedAmountNode.setProperty(CatalogWriteConstants.AMOUNT,
													accountingItemSpecification.getFixedAmount().getAmount());
											fixedAmountNode.setProperty(CatalogWriteConstants.CURRENCY,
													accountingItemSpecification.getFixedAmount().getCurrency());
										}
										if (accountingItemSpecification.getOffsetAmount() != null) {
											Node offsetAmountNode = accountingItemSpecificationNode.addNode(
													CatalogWriteConstants.OFFSET_AMOUNT,
													CatalogWriteConstants.NT_UNSTRUCTURED);
											offsetAmountNode.setProperty(CatalogWriteConstants.AMOUNT,
													accountingItemSpecification.getOffsetAmount().getAmount());
											offsetAmountNode.setProperty(CatalogWriteConstants.CURRENCY,
													accountingItemSpecification.getOffsetAmount().getCurrency());
										}
										if (accountingItemSpecification.getRevenueRecognition() != null)
											accountingItemSpecificationNode.setProperty(
													CatalogWriteConstants.REVENUE_RECOGNITION_TYPE,
													accountingItemSpecification.getRevenueRecognition().value());
									}
								}
							}
						}
					}
					if (priceItemSpecification.getTaxClass() != null) {
						Node taxRateClassNode = priceItemSpecificationNode.addNode(
								priceItemSpecification.getTaxClass().getName(), CatalogWriteConstants.NT_UNSTRUCTURED);
						entitySpecificationSetProperty(taxRateClassNode, priceItemSpecification.getTaxClass());
					}
					ValueExpression valueExpression = priceItemSpecification.getValueExpression();
					if (valueExpression != null) {
						// 19/6
						valueExpression(priceItemSpecificationNode, valueExpression);
					}
					List<PriceItemSpecification> referencedPriceItemList = priceItemSpecification
							.getReferencedPriceItem();
					if (!referencedPriceItemList.isEmpty()) {
						for (PriceItemSpecification referencedPriceItemSpec : referencedPriceItemList) {
							Node referencedPriceItemNode = priceItemSpecificationNode
									.addNode(referencedPriceItemSpec.getName(), CatalogWriteConstants.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(referencedPriceItemNode, referencedPriceItemSpec);
							if (referencedPriceItemSpec != null) {
								priceItemSpec(priceItemSpecificationNode, referencedPriceItemList);
							}
						}
					}

					if (priceItemSpecification instanceof ChargeSpecification) {
						ChargeSpecification chargeSpecification = (ChargeSpecification) priceItemSpecification;
						if (chargeSpecification != null) {
							priceItemSpecificationNode.setProperty(CatalogWriteConstants.IS_AMOUNT_EDITABLE,
									chargeSpecification.isAmountEditable());
							Money amount = chargeSpecification.getAmount();
							if (amount != null)
								createAmountNode(priceItemSpecificationNode, amount, CatalogWriteConstants.AMOUNT_NODE);
							SpendingUnit spendingUnits = chargeSpecification.getSpendingUnits();
							if (spendingUnits != null) {
								if (!priceItemSpecificationNode.hasNode(spendingUnits.getName())) {
									Node spendingUnitsNode = priceItemSpecificationNode.addNode(spendingUnits.getName(),
											CatalogWriteConstants.NT_UNSTRUCTURED);
									entitySpecificationSetProperty(spendingUnitsNode, spendingUnits);
								}
							}
							ChargeType chargeType = chargeSpecification.getChargeType();
							if (chargeType != null) {
								Node chargeTypeNode = priceItemSpecificationNode.addNode(chargeType.getName(),
										CatalogWriteConstants.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(chargeTypeNode, chargeType);
							}
							BillingCycleSpecification billingCycle = chargeSpecification.getBillingCycle();
							if (billingCycle != null) {
								Node billingCycleNode = priceItemSpecificationNode
										.addNode(CatalogWriteConstants.BILLING_CYCLE);
								billingCycleNode.setProperty(CatalogWriteConstants.IS_INVOICE,
										billingCycle.isInvoice());
								billingCycleNode.setProperty(CatalogWriteConstants.RECURRING,
										billingCycle.isRecurring());
								if (billingCycle.getPeriodicityUnits() != null)
									billingCycleNode.setProperty(CatalogWriteConstants.PERIODICITY_UNITS,
											billingCycle.getPeriodicityUnits().intValue());
								if (billingCycle.getTotalCycles() != null)
									billingCycleNode.setProperty(CatalogWriteConstants.TOTAL_CYCLES,
											billingCycle.getTotalCycles().intValue());
								if (billingCycle.getPeriodicity() != null) {
									Node periodicityTypeNode = billingCycleNode
											.addNode(billingCycle.getPeriodicity().getName());
									entitySpecificationSetProperty(periodicityTypeNode, billingCycle.getPeriodicity());
								}
							}
							SpendingLimitSpecification spendingLimit = chargeSpecification.getSpendingLimit();
							if (spendingLimit != null) {
								Node spendingLimitNode = priceItemSpecificationNode
										.addNode(CatalogWriteConstants.SPENDING_LIMIT);
								if (spendingLimit.isLimitEditable() != null)
									spendingLimitNode.setProperty(CatalogWriteConstants.IS_LIMIT_EDITABLE,
											spendingLimit.isLimitEditable());
								SpendingLimitRule spendinglimitRule = spendingLimit.getRule();
								if (spendinglimitRule != null) {
									Node spendinglimitRuleNode = spendingLimitNode
											.addNode(CatalogWriteConstants.SPENDING_LIMIT_RULE);
									List<BigDecimal> alertList = spendinglimitRule.getAlert();
									if (!alertList.isEmpty()) {
										for (BigDecimal alert : alertList) {
											Node alertNode = priceItemSpecificationNode
													.addNode(CatalogWriteConstants.ALERT_NODE);
											alertNode.setProperty(CatalogWriteConstants.ALERT, alert.intValue());
										}
									}
									Money limit = spendinglimitRule.getLimit();
									if (limit != null)
										createAmountNode(spendinglimitRuleNode, limit, CatalogWriteConstants.LIMIT);
									ApplicabilityRule applicabilityRule = spendinglimitRule.getApplicability();
									if (applicabilityRule != null)
										applicabilityRuleNode(spendinglimitRuleNode, applicabilityRule);
								}
							}
						}
					} else if (priceItemSpecification instanceof AllowanceSpecification) {
						AllowanceSpecification allowanceSpecification = (AllowanceSpecification) priceItemSpecification;
						if (allowanceSpecification != null) {
							priceItemSpecificationNode.setProperty(CatalogWriteConstants.IS_TOTAL_ALLOWANCE_EDITABLE,
									allowanceSpecification.isTotalAllowanceEditable());
							BigInteger totalSpendingUnits = allowanceSpecification.getTotalSpendingUnits();
							if (totalSpendingUnits != null)
								priceItemSpecificationNode.setProperty(CatalogWriteConstants.TOTAL_SPENDING_UNITS,
										totalSpendingUnits.longValue());
							if (allowanceSpecification.getTotalFinancial() != null)
								createAmountNode(priceItemSpecificationNode, allowanceSpecification.getTotalFinancial(),
										CatalogWriteConstants.TOTAL_FINANCIAL);
							SpendingUnit spendingUnits = allowanceSpecification.getSpendingUnits();
							if (spendingUnits != null) {
								Node spendingUnitsNode = priceItemSpecificationNode.addNode(spendingUnits.getName(),
										CatalogWriteConstants.NT_UNSTRUCTURED);
								entitySpecificationSetProperty(spendingUnitsNode, spendingUnits);
							}
							AllowancePeriodicity periodicity = allowanceSpecification.getPeriodicity();
							if (periodicity != null) {
								Node periodicityNode = priceItemSpecificationNode
										.addNode(CatalogWriteConstants.PERIODICITY_NODE);
								if (periodicity.getPeriodicityUnits() != null)
									periodicityNode.setProperty(CatalogWriteConstants.PERIODICITY_UNITS,
											periodicity.getPeriodicityUnits().intValue());
								if (periodicity.getTotalCycles() != null)
									periodicityNode.setProperty(CatalogWriteConstants.TOTAL_CYCLES,
											periodicity.getTotalCycles().intValue());
								if (periodicity.isRecurring() != null)
									periodicityNode.setProperty(CatalogWriteConstants.RECURRING,
											periodicity.isRecurring());
								if (periodicity.getPerioditicy() != null) {
									Node periodicityTypeNode = periodicityNode
											.addNode(periodicity.getPerioditicy().getName());
									entitySpecificationSetProperty(periodicityTypeNode, periodicity.getPerioditicy());
								}
							}
							List<BigDecimal> alertList = allowanceSpecification.getAlert();
							if (!alertList.isEmpty()) {
								for (BigDecimal alert : alertList) {
									Node alertNode = priceItemSpecificationNode
											.addNode(CatalogWriteConstants.ALERT_NODE);
									alertNode.setProperty(CatalogWriteConstants.ALERT, alert.intValue());
								}
							}
							if (allowanceSpecification.isFlatRate() != null)
								priceItemSpecificationNode.setProperty(CatalogWriteConstants.FLAT_RATE,
										allowanceSpecification.isFlatRate());
							ApplicabilitySpecification overUse = allowanceSpecification.getApplicability();
							if (overUse != null) {
								applicabilitySpecNode(priceItemSpecificationNode, overUse);
							}
							AllowanceGroupRelationshipSpecification referencedAllowanceGroup = allowanceSpecification
									.getReferencedAllowanceGroup();
							if (referencedAllowanceGroup != null) {
								Node referencedAllowanceGroupNode = priceItemSpecificationNode
										.addNode(CatalogWriteConstants.REFERENCED_ALLOWANCE_GROUP);
								referencedAllowanceGroupNode.setProperty(CatalogWriteConstants.SPENDING_UNITS_FOR_PRICE,
										referencedAllowanceGroup.getSpendingUnitsForPrice().intValue());
								Money price = referencedAllowanceGroup.getPrice();
								if (price != null)
									createAmountNode(referencedAllowanceGroupNode, price, CatalogWriteConstants.PRICE);
								AllowanceGroupSpecification allowanceGroupSpecification = referencedAllowanceGroup
										.getGroup();
								if (allowanceGroupSpecification != null)
									createAllowancegroupSpecNode(referencedAllowanceGroupNode,
											allowanceGroupSpecification);
							}
							RolloverSpecification rolloverSpecification = allowanceSpecification
									.getRolloverSpecification();
							if (rolloverSpecification != null) {
								Node rolloverSpecificationNode = priceItemSpecificationNode
										.addNode(CatalogWriteConstants.ROLL_OVER);
								rolloverSpecificationNode.setProperty(CatalogWriteConstants.PERIODICITY_UNITS,
										rolloverSpecification.getPeriodicityUnits().intValue());
								if (rolloverSpecification.getPeriodicity() != null) {
									Node periodicityTypeNode = rolloverSpecificationNode
											.addNode(rolloverSpecification.getPeriodicity().getName());
									entitySpecificationSetProperty(periodicityTypeNode,
											rolloverSpecification.getPeriodicity());
								}
							}
						}
					} else if (priceItemSpecification instanceof DiscountSpecification) {
						DiscountSpecification discountSpecification = (DiscountSpecification) priceItemSpecification;
						if (discountSpecification != null) {
							priceItemSpecificationNode.setProperty(CatalogWriteConstants.DISCOUNT,
									discountSpecification.getDiscount());
						}
					} else if (priceItemSpecification instanceof CommissionSpecification) {
						CommissionSpecification commissionSpecification = (CommissionSpecification) priceItemSpecification;
						Money amount = commissionSpecification.getAmount();
						if (amount != null)
							createAmountNode(priceItemSpecificationNode, amount, CatalogWriteConstants.AMOUNT_NODE);
						if (commissionSpecification.getCommissionEvent() != null)
							priceItemSpecificationNode.setProperty(CatalogWriteConstants.COMMISSION_EVENT,
									commissionSpecification.getCommissionEvent());
						if (commissionSpecification.getCommissionType() != null)
							priceItemSpecificationNode.setProperty(CatalogWriteConstants.COMMISSION_TYPE,
									commissionSpecification.getCommissionType());
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the AllowancegroupSpecNode and set the related
	 * properties
	 * 
	 * Passing node ,allowanceGroupSpecification
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param allowanceGroupSpecification
	 *            object is used to hold the allowanceGroupSpecification data .
	 * 
	 * @return void
	 * 
	 */

	private void createAllowancegroupSpecNode(Node node, AllowanceGroupSpecification allowanceGroupSpecification) {
		try {
			Node allowanceGroupSpecificationNode = node.addNode(allowanceGroupSpecification.getName());
			entitySpecificationSetProperty(allowanceGroupSpecificationNode, allowanceGroupSpecification);
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the Nodes and properties which are related to
	 * EligibilityRule
	 * 
	 * Passing node ,spec
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param spec
	 *            object is used to hold the specification data .
	 * 
	 * @return void
	 * 
	 */

	protected void createEligibilityRuleNode(Node node, Specification spec) {
		try {
			List<EligibilityRule> eligibilityRuleList = null;
			if (spec instanceof ProductOfferingSpecification) {
				eligibilityRuleList = ((ProductOfferingSpecification) spec).getEligibilityRule();
			} else if (spec instanceof PricePlanSpecification) {
				eligibilityRuleList = ((PricePlanSpecification) spec).getEligibilityRule();
			}
			if (!eligibilityRuleList.isEmpty()) {
				Node eligibilityRulesNode = node.addNode(CatalogWriteConstants.ELIGIBILITY_RULES_NODE);
				eligibilityRulesNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.ELIGIBILITY_RULES);
				for (EligibilityRule eligibilityRule : eligibilityRuleList) {
					Node eligibilityRuleNode = eligibilityRulesNode.addNode(eligibilityRule.getName(),
							CatalogWriteConstants.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(eligibilityRuleNode, eligibilityRule);
					ConditionExpression conditionexpressionOfEligibility = eligibilityRule.getConditionExpression();
					if (conditionexpressionOfEligibility != null) {
						createConditionExpressionNode(eligibilityRuleNode, conditionexpressionOfEligibility);
					}
					List<CustomerSegment> customerSegmentList = eligibilityRule.getCustomerSegment();
					if (!customerSegmentList.isEmpty()) {
						for (CustomerSegment customerSegment : customerSegmentList) {
							Node customerSegmentNode = eligibilityRuleNode.addNode(customerSegment.getName());
							List<MarketSegment> marketSegmentList = customerSegment.getMarketSegment();
							if (marketSegmentList != null) {
								for (MarketSegment marketSegmentObj : marketSegmentList) {
									Node marketSegmentNode = customerSegmentNode.addNode(marketSegmentObj.getName(),
											CatalogWriteConstants.NT_UNSTRUCTURED);
									entitySpecificationSetProperty(marketSegmentNode, marketSegmentObj);
								}
							}
						}
					}
					List<MarketSegment> marketSegment = eligibilityRule.getMarketSegment();
					if (!marketSegment.isEmpty()) {
						for (MarketSegment marketSegmentObj : marketSegment) {
							Node marketSegmentNode = eligibilityRuleNode.addNode(marketSegmentObj.getName(),
									CatalogWriteConstants.NT_UNSTRUCTURED);
							entitySpecificationSetProperty(marketSegmentNode, marketSegmentObj);
						}
					}
					List<SellingChannel> sellingChannelList = eligibilityRule.getSellingChannel();
					if (!sellingChannelList.isEmpty()) {
						for (SellingChannel sellingChannel : sellingChannelList) {
							eligibilityRuleNode.addNode(sellingChannel.getName(),
									CatalogWriteConstants.NT_UNSTRUCTURED);
						}
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	// 14/6
	/**
	 * This method is used to add the nodes and properties which are related to
	 * relationshipSpecification.
	 * 
	 * Passing referencedSpecNode ,relationshipSpecification
	 * 
	 * @param referencedSpecNode
	 *            is used to add the property .
	 * @param priceItemSpec
	 *            object is used to hold the relationshipSpecification data .
	 * @param pathDataList
	 *            is used to hold the exact location of related specification
	 *            node where we need to set the absolute path of related
	 *            specification .
	 * 
	 * @return void
	 * 
	 */
	protected void relationshipSpecification(Node referencedSpecNode,
			RelationshipSpecification relationshipSpecification, List<PathModel> pathDataList) {
		try {
			referencedSpecNode.setProperty(CatalogWriteConstants.MIN_MULTIPICITY,
					relationshipSpecification.getMinMultiplicity().longValue());
			// 12/6
			if (relationshipSpecification.getMaxMultiplicity() != null) {
				referencedSpecNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
						relationshipSpecification.getMaxMultiplicity().longValue());
			}
			List<Specification> relatedSpecificationList = relationshipSpecification.getRelatedSpecification();
			if (!relatedSpecificationList.isEmpty()) {
				Node relatedSpecsNode = referencedSpecNode.addNode(CatalogWriteConstants.RELATED_SPECIFICATIONS,
						CatalogWriteConstants.NT_UNSTRUCTURED);
				relatedSpecificationList.forEach(relatedSpecification -> {
					try {
						// Node relatedSpecificationNode =
						// referencedSpecNode.addNode(relatedSpecification.getName());
						PathModel pathModel = new PathModel();
						Node relatedSpecificationNode = relatedSpecsNode.addNode(relatedSpecification.getName());
						pathModel.setAbsolutePath(relatedSpecificationNode.getPath());
						pathModel.setName(relatedSpecification.getName());
						pathModel.setPathNode(relatedSpecificationNode);
						pathDataList.add(pathModel);
						entitySpecificationSetProperty(relatedSpecificationNode, relatedSpecification);
						// 14/6
						if (CatalogWriteConstants.TYPE.equals(CatalogWriteConstants.PRODUCT_OFFERING)) {
							relatedSpecificationNode.setProperty(CatalogWriteConstants.TYPE,
									CatalogWriteConstants.SUB_PRODUCT_OFFERING);
						}
						specificationSetProperty(relatedSpecificationNode, relatedSpecification, pathDataList);
					} catch (Exception ex) {
						log.info("Error:" + ex.getLocalizedMessage());
						errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
								FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
					}

				});
			}
		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
	}

}
