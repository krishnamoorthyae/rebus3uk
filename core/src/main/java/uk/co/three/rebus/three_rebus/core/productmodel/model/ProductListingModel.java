/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.model;

import java.io.Serializable;

/**
 * This is the model class to represent product data to be retrieved/shown in 
 * product listing page
 * 
 * 
 *
 */
public class ProductListingModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * TODO
	 * need to define the data variables in this class
	 * we need functional specification
	 */
	private String name;
	
	private String price;
	
	private String rating;
	
	private String model;
	
	private String description;
	
	private String upFrontPrice;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpFrontPrice() {
		return upFrontPrice;
	}

	public void setUpFrontPrice(String upFrontPrice) {
		this.upFrontPrice = upFrontPrice;
	}
	
	
}
