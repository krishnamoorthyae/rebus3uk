package uk.co.three.rebus.three_rebus.core.productmodel.service;

import uk.co.three.rebus.three_rebus.core.productmodel.request.GetCategoryAndProductsRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetCategoryAndProductsResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetProductsResponse;


/**
 * 
 * @author DD00493288
 *
 */
public interface GetCategoriesAndProducts {
	
	public GetCategoryAndProductsResponse getCategories(GetCategoryAndProductsRequest getCategoryRequest);
	public GetProductsResponse getProducts(GetCategoryAndProductsRequest getCategoryRequest);

}
