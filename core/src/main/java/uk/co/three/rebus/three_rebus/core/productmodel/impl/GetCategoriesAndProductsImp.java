package uk.co.three.rebus.three_rebus.core.productmodel.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epc.h3g.com.Category;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.productmodel.model.ProductListingModel;
import uk.co.three.rebus.three_rebus.core.productmodel.request.GetCategoryAndProductsRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetCategoryAndProductsResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetProductsResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.service.GetCategoriesAndProducts;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

@Component
@Service(value = { GetCategoriesAndProducts.class })
public class GetCategoriesAndProductsImp implements GetCategoriesAndProducts {
	private static final String CATEGORY_QUERY = "SELECT node.* FROM [nt:base] AS node WHERE ISCHILDNODE([%s]) and (nodeType='CATEGORY' or nodeType='PRODUCT_OFFERING')";
	private static final String PRODUCT_QUERY = "SELECT node.* FROM [nt:base] AS node WHERE ISDESCENDANTNODE([%s]) and (name LIKE '%s' and nodeType='PRODUCT_OFFERING')";
	private static final String PRICE_PLAN_QUERY = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([%s]) and nodeType='PRICE_PLAN'";
	private static final String PRICE_QUERY = "SELECT node.* FROM [nt:base] AS node WHERE ISDESCENDANTNODE([%s]) and node.amount IS NOT NULL";

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private Map<Integer, String> errorMap = new HashMap<Integer, String>();

	@Override
	public GetCategoryAndProductsResponse getCategories(GetCategoryAndProductsRequest getCategoryRequest) {
		GetCategoryAndProductsResponse categoryResponse = new GetCategoryAndProductsResponse();
		List<uk.co.three.rebus.three_rebus.core.productmodel.model.Category> subCategories = new LinkedList<uk.co.three.rebus.three_rebus.core.productmodel.model.Category>();
		List<ProductListingModel> productList = new LinkedList<ProductListingModel>();
		try {
			String getCategoryQuery = String.format(CATEGORY_QUERY, getCategoryRequest.getNodePath());
			Session session = getCategoryRequest.getSession();
			NodeIterator nodes = getResultNodes(session, getCategoryQuery);
			while (nodes.hasNext()) {
				Node node = nodes.nextNode();
				String nodeType = node.getProperty(RebusUtils.TYPE).getValue().getString();
				if (RebusUtils.CATEGORY.equalsIgnoreCase(nodeType)) {
					uk.co.three.rebus.three_rebus.core.productmodel.model.Category subcategory = getSubCategory(node);
					subCategories.add(subcategory);
				} else if (RebusUtils.PRODUCT_OFFERING.equalsIgnoreCase(nodeType)) {
					ProductListingModel offeringSpecification = getProduct(session, node);
					productList.add(offeringSpecification);
				}
			}

		} catch (Exception e) {
			log.info("Exception while fetching categories and products:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		}
		categoryResponse.setSubCategories(subCategories);
		categoryResponse.setProductList(productList);
		setErrorMessages(null, categoryResponse, errorMap);
		return categoryResponse;

	}

	private uk.co.three.rebus.three_rebus.core.productmodel.model.Category getSubCategory(Node childNode)
			throws RepositoryException {
		Category epcCategory = new Category();
		PropertyIterator propertyIterator = childNode.getProperties();
		while (propertyIterator.hasNext()) {
			Property property = propertyIterator.nextProperty();
			String value = property.getValue().getString();
			String name = property.getName();
			if (RebusUtils.CATALOG_ID.equalsIgnoreCase(name)) {
				epcCategory.setCatalogId(value);
			} else if (RebusUtils.NAME.equalsIgnoreCase(name)) {
				epcCategory.setName(value);
			} else if (RebusUtils.TYPE.equalsIgnoreCase(name)) {
				epcCategory.setType(value);
			}
		}
		return new uk.co.three.rebus.three_rebus.core.productmodel.model.Category(epcCategory);
	}

	private ProductListingModel getProduct(Session session, Node productNode) throws RepositoryException {
		ProductListingModel productModel = new ProductListingModel();
		PropertyIterator iterator = productNode.getProperties();
		while (iterator.hasNext()) {
			Property property = iterator.nextProperty();
			String propertyName = property.getName().toString();
			String propertyValue = property.getValue().toString();
			if (RebusUtils.NAME.equals(propertyName)) {
				productModel.setName(propertyValue);
			} else if (RebusUtils.DESCRIPTION.equalsIgnoreCase(propertyName)) {
				productModel.setDescription(propertyValue);
			} else if (RebusUtils.MODEL.equalsIgnoreCase(propertyName)) {
				productModel.setModel(propertyValue);
			} else if (RebusUtils.RATING.equalsIgnoreCase(propertyName)) {
				productModel.setRating(propertyValue);
			} else if (RebusUtils.UPFRONT_AMOUNT.equalsIgnoreCase(propertyName)) {
				productModel.setUpFrontPrice(propertyValue);
			}
		}
		productModel.setPrice(getPrice(session, productNode));
		return productModel;
	}

	private String getPrice(Session session, Node productNode) throws RepositoryException {
		String price = null;
		NodeIterator nodes = getResultNodes(session, String.format(PRICE_PLAN_QUERY, productNode.getPath()));
		while (nodes.hasNext()) {
			Node node = nodes.nextNode();
			price = getAmount(session, node);
			break;
		}
		return price;
	}

	private String getAmount(Session session, Node node) throws RepositoryException {
		String price = null;
		if (RebusUtils.ACTIVE.equalsIgnoreCase(node.getProperty(RebusUtils.STATUS).getValue().getString())) {
			NodeIterator nodes = getResultNodes(session, String.format(PRICE_QUERY, node.getPath()));
			while (nodes.hasNext()) {
				Node amountNode = nodes.nextNode();
				if (RebusUtils.AMOUNT.equalsIgnoreCase(amountNode.getName())) {
					price = String.valueOf(amountNode.getProperty(RebusUtils.AMOUNT).getValue().getDecimal());
					break;
				}
			}
		}
		return price;

	}

	private NodeIterator getResultNodes(Session session, String queryString)
			throws RepositoryException, InvalidQueryException {
		QueryManager queryManager = session.getWorkspace().getQueryManager();
		Query query = queryManager.createQuery(queryString, Query.JCR_SQL2);
		QueryResult result = query.execute();
		NodeIterator nodes = result.getNodes();
		return nodes;
	}

	private void setErrorMessages(GetProductsResponse productResponse, GetCategoryAndProductsResponse categoryResponse,
			Map<Integer, String> errorMap) {
		Set<Integer> errorCodes = errorMap.keySet();
		if (errorCodes != null) {
			for (Integer code : errorCodes) {
				if (productResponse != null) {
					productResponse.addErrorObject(code, errorMap.get(code));
				} else {
					categoryResponse.addErrorObject(code, errorMap.get(code));
				}
			}
		}
	}

	@Override
	public GetProductsResponse getProducts(GetCategoryAndProductsRequest getCategoryRequest) {
		GetProductsResponse productResponse = new GetProductsResponse();
		List<ProductListingModel> productList = new LinkedList<ProductListingModel>();
		try {
			Session session = getCategoryRequest.getSession();
			NodeIterator nodeIterator = getResultNodes(session, String.format(PRODUCT_QUERY, getCategoryRequest.getNodePath(), getCategoryRequest.getProductName()));
			while (nodeIterator.hasNext()) {
				Node node = nodeIterator.nextNode();
				productList.add(getProduct(session, node));
			}

		} catch (RepositoryException e) {
			log.info("Exception while fetching products:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
		productResponse.setProductList(productList);
		setErrorMessages(productResponse, null, errorMap);
		return productResponse;
	}

}
