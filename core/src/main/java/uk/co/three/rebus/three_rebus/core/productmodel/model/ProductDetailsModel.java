/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.model;

import java.io.Serializable;

import epc.h3g.com.ProductOfferingSpecification;

/**
 * This model class is to be used for showing the required product details in
 * the product details page This class internally contains the product offering
 * specification object
 * 
 * @author DD00493288
 *
 */
public class ProductDetailsModel implements Serializable {

	/**
	 * Stores the EPC product offering specification
	 */
	private ProductOfferingSpecification productOfferingSpecification = null;

	/**
	 * @param productOfferingSpecification
	 */
	public ProductDetailsModel(ProductOfferingSpecification productOfferingSpecification) {
		this.productOfferingSpecification = productOfferingSpecification;
	}
	
	/**
	 * TODO
	 * there is a need to specify all the getter methods as per functional specification
	 */
	

}
