package uk.co.three.rebus.three_rebus.core.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;


import uk.co.three.rebus.three_rebus.core.services.FetchProductDetails;

@SlingServlet(paths="/bin/intermediate/info")
public class IntermediateProductServlet extends SlingAllMethodsServlet {
	
	@Reference
	FetchProductDetails fetchProductDetails;
	private static final long serialVersionUID = 1L;
	
	
	
	
	public void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse){
		
		try {
			slingResponse.getWriter().println("Param:"+slingRequest.getParameter("id"));
			String requestParam = slingRequest.getParameter("id");
			 //fetchProductDetails.getProductDetails(requestParam); 
			ArrayList<String> responseList = new ArrayList<String>();
			responseList = fetchProductDetails.getProductDetails(requestParam);
			//response.getWriter().println(fetchProductDetails.getProductDetails(requestParam));
			
			slingRequest.setAttribute("responseProductList", responseList);
			//slingRequest.getRequestDispatcher("/content/product-details.html").forward(slingRequest, slingResponse);
			slingRequest.getRequestDispatcher("/content/three-product-details.html").forward(slingRequest, slingResponse);
			//slingResponse.sendRedirect("/content/three-product-details.html");
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
}
