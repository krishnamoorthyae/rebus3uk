package uk.co.three.rebus.three_rebus.core.ui.sightly;

import java.util.List;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Reference;

import com.adobe.cq.sightly.WCMUse;

import epc.h3g.com.ProductOfferingSpecification;
//import uk.co.three.rebus.three_rebus.core.models.ProductOfferingNodeData;
import uk.co.three.rebus.three_rebus.core.productmodel.service.SessionHandlerService;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider1;
//import uk.co.three.rebus.three_rebus.core.service.QueryProvider;

public class ReadRepositoryData extends WCMUse {

	public String detail;
	
	
	@Override
	public void activate() throws Exception {
		// TODO Auto-generated method stub

QueryProvider1 queryProviderImp = getSlingScriptHelper().getService(QueryProvider1.class);
		
SessionHandlerService sessionHandlerService=getSlingScriptHelper().getService(SessionHandlerService.class);


	// we can populate JAX B Object values 
	//ProductOfferingSpecification productOfferingSpecification=queryProviderImp.getProductOfferSample(sessionHandlerService.openSession(), "Three Essential Plan (4GB data, 600 minutes)");
		//ProductOfferingSpecification productOfferingSpecification=queryProviderImp.getProductOfferDetails2(sessionHandlerService.openSession(), "Three Essential Plan (4GB data, 600 minutes)");
	List<ProductOfferingSpecification> productOfferingSpecification=queryProviderImp.getProductOfferSample(sessionHandlerService.openSession(), "Three Essential Plan (4GB data, 600 minutes)");	
	detail=productOfferingSpecification.get(0).getCatalogId();
	getResourceResolver().adaptTo(Session.class).logout();
	/*for(ProductOfferingSpecification pos:productOfferingSpecification)
		{
			detail = pos.getCatalogId();
		}
	*/
	}

	public String getDetails() {
		return this.detail;
	}
	
	
}
