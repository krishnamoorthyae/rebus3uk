package uk.co.three.rebus.three_rebus.core.filters;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

@SlingFilter(
	label = "Add query parameter to OmniScriptPath",
	description = "Filter to Add query parameter to OmniScriptPath", 
	order = 0,
	scope = SlingFilterScope.REQUEST)
@Properties({
	@Property(name="sling.filter.pattern", value="/bin/ospath")
})
public class AzureOSPathFilter implements Filter{

	@Override
	public void destroy() {
		 
		
	}
	
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain arg2)
			throws IOException, ServletException {
		 	response.getWriter().println("Sling Servlet");
		 	SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
		 	SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
		 	
		 	ResourceResolver resourceResolver = slingRequest.getResourceResolver();
		 	Resource resource = slingRequest.getResource();
		 	String url = request.getParameter("redirectUrl");
		 	String redirectUrl = url+".html";
		 	//Node node = resourceResolver.getResource("/content/geometrixx/en/services/purchasesummary/jcr:content").adaptTo(Node.class);
		 	Node node = resourceResolver.getResource(url+"/jcr:content").adaptTo(Node.class);
		 	try {
		 			
		 		/* if(node.hasNode("par")){
							Node parsys = node.getNode("par");*/	
							if(node.hasNode("body-content")){
							Node parsys = node.getNode("body-content");	
							if(parsys.hasNodes()){
								NodeIterator components = parsys.getNodes();
								while(components.hasNext()){
										Node authorComponent = components.nextNode();
										if(authorComponent.getName().contentEquals("omniuniversalcont")){
											response.getWriter().println(authorComponent.getName());
											PropertyIterator property = authorComponent.getProperties();
												while(property.hasNext()){
													//response.getWriter().println(property.nextProperty());
													javax.jcr.Property propertyValue = property.nextProperty();
													if(propertyValue.getName().contentEquals("osFullPath")){
														String osPath = propertyValue.getValue().toString();
														//propertyValue.setValue("?prodID="+request.getParameter("pid")+"&continueShopURL=/content/3uk/sightly/SightlyPage.html"+"&redirectURL=/content/3uk/sightly/SightlyPage.html"+"#/OmniScriptType/POC Omniscripts/OmniScriptSubType/AEM POC/OmniScriptLang/English/ContextId//PrefillDataRaptorBundle//false");
														//propertyValue.setValue("?prodID="+request.getParameter("pid")+"&continueShopURL=/content/three-product-listing.html"+"&redirectURL=/content/checkout.html"+"#/OmniScriptType/POC Omniscripts/OmniScriptSubType/AEM POC/OmniScriptLang/English/ContextId//PrefillDataRaptorBundle//false");
														propertyValue.setValue("?prodID="+request.getParameter("pid")+"&continueShopURL=/content/three-product-list.html"+"#/OmniScriptType/POC Omniscripts/OmniScriptSubType/AEM POC/OmniScriptLang/English/ContextId//PrefillDataRaptorBundle//false");
														response.getWriter().println(propertyValue.getValue()); 
													}
												}
										}
								}
							}
							node.getSession().save();
					}
					slingResponse.sendRedirect(redirectUrl);
				//slingResponse.sendRedirect("/content/geometrixx/en/services/purchasesummary.html");
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		 
		
	}

}
