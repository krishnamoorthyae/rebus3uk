package uk.co.three.rebus.three_rebus.core.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.HttpHeaders;

@SlingServlet(paths="/bin/customservlet/authenticate", methods = { "POST", "GET" })
public class WebServerOAuthFlow extends SlingAllMethodsServlet {
	
		/*protected String username = "integration@bluewolfgroup.com.three.omniout";
		protected String password = "Three2017";*/
		protected String clientId = "3MVG9lcxCTdG2VbvRv1iNHDyMCJNHab.UIj2KuSUckj2Nqawih6U5EG.BEh0RNavAQjBNI5URE3r3Tq_0S7AV";
		protected String secret = "8620885673208600844";
		protected String loginHost = "https://test.salesforce.com";
		protected String value = null;
		protected String key = null;
		protected String errorValue =null;
		protected String baseUrl = loginHost + "/services/oauth2/token";
		protected JSONObject authResponse;
		
		private static final long serialVersionUID = 1L;
	
		private Logger LOGGER = LoggerFactory.getLogger(WebServerOAuthFlow.class);
	
		private Map<String, String> jsonResponseMap = new HashMap<String, String>();
	
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		//Session session = request.getResourceResolver().adaptTo(Session.class);
		String username = request.getParameter("username");
		 String password = request.getParameter("pswd");
		//response.getWriter().println(baseUrl);
		//response.getWriter().println(username+password);
			HttpClient client = new HttpClient();
			//HttpParams params = client.getParams();
		    PostMethod post = new PostMethod(baseUrl);
			post.addParameter("username", username);
			post.addParameter("password", password);
			post.addParameter("grant_type", "password");
			post.addParameter("client_id", clientId);
			post.addParameter("client_secret", secret);
			client.executeMethod(post);
				try {
					//response.getWriter().println("post"+post.getStatusCode()+post.getStatusText());
					if(post.getStatusCode() == 200 ){
						//response.getWriter().println("200");
							authResponse = new JSONObject(post.getResponseBodyAsString());
							Iterator<String> itr = authResponse.keys();
							LOGGER.info("JSON Length:"+authResponse.length());
							while(itr.hasNext()){
								String jsonKey = (String) itr.next();
								String jsonValue = authResponse.getString(jsonKey);
								LOGGER.info("*****************AUTH CALL SUCCESSFUL********************");
								jsonResponseMap.put(jsonKey, jsonValue);
								LOGGER.info("JSON KEYS:"+jsonKey);
							}
							LOGGER.info("JSON SIZE:"+jsonResponseMap.size());
							for(Map.Entry<String, String> jK:jsonResponseMap.entrySet()){
								response.getWriter().println("JSON RESPONSE"+jK.getKey()+":"+jK.getValue());
							}
							
							Cookie ck = new Cookie("sfdc_session_id",jsonResponseMap.get("access_token"));
							ck.setMaxAge(-1);
							ck.setPath("/");
							//ck.setSecure(true);
							//ck.setMaxAge(3600);
							response.addCookie(ck);
							Cookie ck1 = new Cookie("sfdc_instance_url",jsonResponseMap.get("instance_url"));
							ck1.setMaxAge(-1);
							ck1.setPath("/");
							response.addCookie(ck1);
							if(request.getParameter("redirecturl") != null){
								response.sendRedirect(request.getParameter("redirecturl"));
							}else{
								response.sendRedirect("/content/three-product-listing.html");
							}
							
					}else{
						response.getWriter().println("Please Check the Credentials Entered");
						
					}
					/*LOGGER.info("Call to Fetch Details");
					String  URL = request.getParameter("url");
					GetMethod get = new GetMethod(URL);
					get.addRequestHeader(HttpHeaders.AUTHORIZATION, "Bearer "+jsonResponseMap.get("access_token"));
					get.addRequestHeader(HttpHeaders.CONTENT_TYPE,"application/json");
					client.executeMethod(get);
					JSONObject secResponse;
					secResponse = new JSONObject(get.getResponseBodyAsString());
					LOGGER.info("Response:"+secResponse);
					response.getWriter().println("Response:"+secResponse);
					get.releaseConnection();*/
				} catch (JSONException e) {
					LOGGER.info("JSONEXCEPTION"+e);
				}
				
				 post.releaseConnection();	
		}
	

}
