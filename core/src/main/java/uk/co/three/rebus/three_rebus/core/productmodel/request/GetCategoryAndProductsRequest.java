package uk.co.three.rebus.three_rebus.core.productmodel.request;

import javax.jcr.Session;

import uk.co.three.rebus.three_rebus.core.request.BaseRequest;

public class GetCategoryAndProductsRequest extends BaseRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nodePath;
	
	private String productName;
	
	private Session session;

	public String getNodePath() {
		return nodePath;
	}

	public void setNodePath(String filePath) {
		this.nodePath = filePath;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}
	

}
