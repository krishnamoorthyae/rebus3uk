package uk.co.three.rebus.three_rebus.core.productmodel.response;

import java.util.List;

import uk.co.three.rebus.three_rebus.core.productmodel.model.ProductListingModel;
import uk.co.three.rebus.three_rebus.core.response.BaseResponse;

public class GetProductsResponse extends BaseResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ProductListingModel> productList;

	/**
	 * @return the productList
	 */
	public List<ProductListingModel> getProductList() {
		return productList;
	}

	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<ProductListingModel> productList) {
		this.productList = productList;
	}
	
	
	
	

}
