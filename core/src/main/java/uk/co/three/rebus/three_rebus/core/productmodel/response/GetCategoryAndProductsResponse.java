package uk.co.three.rebus.three_rebus.core.productmodel.response;

import java.util.List;

import uk.co.three.rebus.three_rebus.core.productmodel.model.ProductListingModel;
import uk.co.three.rebus.three_rebus.core.response.BaseResponse;

public class GetCategoryAndProductsResponse extends BaseResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<uk.co.three.rebus.three_rebus.core.productmodel.model.Category> subCategories;
	
	private List<ProductListingModel> productList;

	public List<uk.co.three.rebus.three_rebus.core.productmodel.model.Category> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<uk.co.three.rebus.three_rebus.core.productmodel.model.Category> subCategories) {
		this.subCategories = subCategories;
	}

	public List<ProductListingModel> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductListingModel> productList) {
		this.productList = productList;
	}

	
	

	
	

}
