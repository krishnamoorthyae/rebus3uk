/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.response;

import uk.co.three.rebus.three_rebus.core.response.BaseResponse;
import epc.h3g.com.ProductOfferingSpecification;

/**
 * This is the response class for getting all the products
 * This class will contain the list of product offering specifications
 * @author DD00493288
 *
 */
public class GetProductDetailsResponse extends BaseResponse {

	
	
	private ProductOfferingSpecification product ;

	public ProductOfferingSpecification getProduct() {
		return product;
	}

	public void setProduct(ProductOfferingSpecification product) {
		this.product = product;
	}

	
	
}
