package uk.co.three.rebus.three_rebus.core.productmodel.request;

import java.io.Serializable;

import uk.co.three.rebus.three_rebus.core.request.BaseRequest;

public class CatalogRequest extends BaseRequest implements Serializable {

	private String resourcePath = "";

	public CatalogRequest() {

	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

}
