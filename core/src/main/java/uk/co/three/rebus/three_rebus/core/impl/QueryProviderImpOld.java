package uk.co.three.rebus.three_rebus.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

import epc.h3g.com.ProductOfferingSpecification;
//import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.service.QueryProvider1;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

@Component(label = "Title", description = "Description.")
@Service(value = { QueryProvider1.class })
public class QueryProviderImpOld implements QueryProvider1 {
	
	@Reference
	private QueryBuilder queryBuilder;

	@Override
	public List<ProductOfferingSpecification> getProductOfferSample(Session session, String productName)
			throws InvalidQueryException, RepositoryException {	
		List<ProductOfferingSpecification> offeringSpecification = new ArrayList<ProductOfferingSpecification>();
		Node node =  session.getNode("/content/three_rebus/products/catalogOne/Mobile/Bundles/Three Essential Plan (4GB data, 600 minutes)");
		Map<String, Integer> map = new HashMap<String, Integer>();
		//map = getAllProperties(map, node);
		getAllProperties(map, node);
		return offeringSpecification;
	}

	/*private Map<String, Integer> getAllProperties(Map<String, Integer> map, Node node) throws RepositoryException {
		map.put(node.getName(), node.getDepth());
		NodeIterator iterator = node.getNodes();
		while(iterator.hasNext()) {
			Node childNode = iterator.nextNode();	
			childNode.getProperties();
			getAllProperties(map,childNode);		
		}
		
		return map;
		
	}*/
	private void getAllProperties(Map<String, Integer> map, Node node) throws RepositoryException {
		map.put(node.getName(), node.getDepth());
		NodeIterator iterator = node.getNodes();
		while(iterator.hasNext()) {
			Node childNode = iterator.nextNode();	
			//System.out.println(childNode.getProperties());
			/*PropertyIterator pi = childNode.getProperties();
			ProductOfferingSpecification productOfferingSpecification=new ProductOfferingSpecification();
			while(pi.hasNext())
			{
			   Property p = pi.nextProperty();
			   String name = p.getName();
			   String value = p.getString();
			   
			}*/
			getAllProperties(map,childNode);		
		}
		
	}

}
