package uk.co.three.rebus.three_rebus.core.models;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

import uk.co.three.rebus.three_rebus.core.services.AzureProductReadServiceImpl;


public class AccessProductDetails extends WCMUsePojo {
Logger LOGGER = LoggerFactory.getLogger(AccessProductDetails.class);
	private Map<String, ArrayList> sightlyMap = new HashMap<String, ArrayList>();
	ArrayList<String> propertyList = new ArrayList<String>();
	private String sampStr;
	private String text;
	private int listSize;
	//String queryStatement = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(["+productPath+"])";
	
	private static String productPath ="/content/three_UK/products";
	@Override
	public void activate() throws Exception {
		LOGGER.info("WCMUSE");
		String queryStatement = "SELECT * FROM [nt:unstructured] AS nodes WHERE ISCHILDNODE(["+productPath+"])";
		text = get("text",String.class);
		if(text != null){
			queryStatement = "SELECT * FROM [nt:unstructured] AS nodes WHERE ISCHILDNODE(["+productPath+"])  AND [owner] ='"+text+"'";;
		}else{
			queryStatement = "SELECT * FROM [nt:unstructured] AS nodes WHERE ISCHILDNODE(["+productPath+"])";
		}
		sampStr = "Value from WCMUSE";
		AzureProductReadServiceImpl azureProductReadService = getSlingScriptHelper().getService(AzureProductReadServiceImpl.class);
		sightlyMap = azureProductReadService.initiateRead(queryStatement); 
		//propertyList = (ArrayList<String>) productReadService.initiateRead();
		listSize = sightlyMap.size();
		LOGGER.info("Sightly:"+propertyList);
	}
	


	public Map getProductDetail(){
		return sightlyMap;
		//return propertyList;
	}
	
	public String getString(){
		return text;
	}
	
	public int getSize(){
		return listSize;
	}

}
