package uk.co.three.rebus.three_rebus.core.productmodel.write;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import epc.h3g.com.ApplicabilityRule;
import epc.h3g.com.ApplicabilitySpecification;
import epc.h3g.com.CharacteristicGroupSpecification;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CharacteristicValue;
import epc.h3g.com.Condition;
import epc.h3g.com.ConditionBracket;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.ConditionExpressionItem;
import epc.h3g.com.ConditionOperator;
import epc.h3g.com.Country;
import epc.h3g.com.EntityContentResource;
import epc.h3g.com.EntitySpecification;
import epc.h3g.com.Localization;
import epc.h3g.com.Location;
import epc.h3g.com.Money;
import epc.h3g.com.Specification;
import epc.h3g.com.SpecificationStatus;
import epc.h3g.com.SpecificationVersion;
import epc.h3g.com.StatusHistory;
import epc.h3g.com.TimePeriod;
import epc.h3g.com.ValueBracket;
import epc.h3g.com.ValueCRM;
import epc.h3g.com.ValueCharacteristic;
import epc.h3g.com.ValueExpression;
import epc.h3g.com.ValueExpressionItem;
import epc.h3g.com.ValueFixed;
import epc.h3g.com.ValueOperator;
import epc.h3g.com.XMLPathSpecification;
import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.productmodel.constants.CatalogWriteConstants;

/**
 * This class is responsible for handling Characteristic data And inserting the
 * data into repository . Its super class of SpecificationWriter.
 * 
 * @author MS00506946
 *
 */

public class CharacteristicWriter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	public Map<Integer, String> errorMap = new HashMap<Integer, String>();

	/**
	 * This method is used to add the nodes and properties which are related to
	 * characteristicSpecification
	 * 
	 * Passing productOfferNode ,listCharSpec ,characteristicSpecification
	 * 
	 * @param productOfferNode
	 *            is used to get the ProductOfferingSpecification object to
	 *            productOfferNode object .
	 * @param listCharSpec
	 *            is used to iterate the List of CharacteristicSpecification
	 * @param characteristicSpecification
	 *            used to hold the characteristicSpecification data .
	 * 
	 * @return void
	 * 
	 */

	protected void characteristicSpecification(Node productOfferNode, CharacteristicSpecification CharSpec) {

		try {
			Node characteristicSpecificationNode = productOfferNode.addNode(CharSpec.getName(),
					CatalogWriteConstants.NT_UNSTRUCTURED);

			entitySpecificationSetProperty(characteristicSpecificationNode, CharSpec);

			/* Mandatory Properties */
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.IS_COMMERICAL, CharSpec.isCommercial());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.IS_EDITABLE, CharSpec.isEditable());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.IS_MANDATORY, CharSpec.isMandatory());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.IS_PRINTABLE, CharSpec.isPrintable());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.IS_VISIBLE, CharSpec.isVisible());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.MIN_MULTIPICITY,
					CharSpec.getMinMultiplicity().longValue());
			characteristicSpecificationNode.setProperty(CatalogWriteConstants.VALUE_TYPE,
					CharSpec.getValueType().value());
			/* BigInteger Properties */
			// 31/5
			if (CharSpec.getMaxMultiplicity() != null) {
				characteristicSpecificationNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
						CharSpec.getMaxMultiplicity().longValue());
			}
			if (CharSpec.getMaxSize() != null) {
				characteristicSpecificationNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
						CharSpec.getMaxSize().longValue());
			}
			if (CharSpec.getScale() != null) {
				characteristicSpecificationNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
						CharSpec.getScale().longValue());
			}
			List<CharacteristicValue> allowedValues = CharSpec.getAllowedValue();
			if (!allowedValues.isEmpty()) {
				Node allowedValueNode = characteristicSpecificationNode.addNode(CatalogWriteConstants.ALLOWED_VALUE);
				List<String> values = new ArrayList<String>();
				for (CharacteristicValue allowedValue : allowedValues) {
					allowedValueNode.setProperty(CatalogWriteConstants.VALUE_TYPE, allowedValue.getValueType().value());

					Element element = (Element) allowedValue.getValue();
					org.w3c.dom.Node node = element.getFirstChild();
					String attrValue = node.getNodeValue();
					values.add(attrValue);
				}
				String[] strings = values.stream().toArray(String[]::new);
				allowedValueNode.setProperty(CatalogWriteConstants.VALUE, strings);
			}
			CharacteristicValue defaultValue = CharSpec.getDefaultValue();
			if (defaultValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, defaultValue,
						CatalogWriteConstants.DEFAULT_VALUE);

			}
			CharacteristicValue maxValue = CharSpec.getMaxValue();
			if (maxValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, maxValue,
						CatalogWriteConstants.MAX_VALUE);

			}
			CharacteristicValue minValue = CharSpec.getMinValue();
			if (minValue != null) {
				createCharacteristicValueNode(characteristicSpecificationNode, minValue,
						CatalogWriteConstants.MIN_VALUE);
			}

		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used for Recursive call to handle the
	 * characteristicGroupSpecification inside the characteristicSpecification
	 * 
	 * Passing node ,specification
	 * 
	 * @param node
	 *            is used to set the property .
	 * @param specification
	 *            is used to get the CharacteristicGroupSpecification object
	 * 
	 * @return void
	 * 
	 */

	protected void createCharacteristicGroupSpecificationNode(Node node,
			CharacteristicGroupSpecification characteristicGroupSpecification) {
		try {
			Node characteristicGroupSpecificationNode = node.addNode(characteristicGroupSpecification.getName());
			// 1/6
			entitySpecificationSetProperty(characteristicGroupSpecificationNode, characteristicGroupSpecification);

			List<CharacteristicSpecification> characteristicSpecifications = characteristicGroupSpecification
					.getCharacteristicSpecifications();
			if (!characteristicSpecifications.isEmpty()) {
				// 22/6
				Node charSpecsNode = characteristicGroupSpecificationNode
						.addNode(CatalogWriteConstants.CHARACTERISTIC_SPECIFICATIONS_NODE);
				charSpecsNode.setProperty(CatalogWriteConstants.TYPE,
						CatalogWriteConstants.CHARACTERISTIC_SPECIFICATIONS);
				for (CharacteristicSpecification charSpec : characteristicSpecifications) {
					characteristicSpecification(characteristicGroupSpecificationNode, charSpec);
				}
			}
			characteristicGroupSpecificationNode.setProperty(CatalogWriteConstants.MIN_MULTIPICITY,
					characteristicGroupSpecification.getMinMultiplicity().longValue());
			if (characteristicGroupSpecification.getMaxMultiplicity() != null) {
				characteristicGroupSpecificationNode.setProperty(CatalogWriteConstants.MAX_MULTIPICITY,
						characteristicGroupSpecification.getMaxMultiplicity().longValue());
			}
			List<CharacteristicGroupSpecification> componentCharGroupSpecList = characteristicGroupSpecification
					.getComponentCharacteristicGroupSpecification();
			if (!componentCharGroupSpecList.isEmpty()) {
				for (CharacteristicGroupSpecification componentGroupSpec : componentCharGroupSpecList) {
					createCharacteristicGroupSpecificationNode(characteristicGroupSpecificationNode,
							componentGroupSpec);
				}
			}

		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used for Recursive call to handle the characteristicValue
	 * inside the characteristicSpecification
	 * 
	 * Passing characteristicSpecificationNode ,characteristicValue ,nodeName
	 * 
	 * @param characteristicSpecificationNode
	 *            is used to set the property .
	 * @param nodeName
	 *            is used to pass the String value
	 * 
	 * @return void
	 * 
	 */
	// 19/6
	protected void createCharacteristicValueNode(Node characteristicSpecificationNode,
			CharacteristicValue characteristicValue, String nodeName) {
		try {
			Node characteristicValueNode = characteristicSpecificationNode.addNode(nodeName);
			characteristicValueNode.setProperty(CatalogWriteConstants.VALUE_TYPE,
					characteristicValue.getValueType().value());
			// 16/6
			Element element = (Element) characteristicValue.getValue();
			org.w3c.dom.Node node = element.getFirstChild();
			String attrValue = node.getNodeValue();
			characteristicValueNode.setProperty(CatalogWriteConstants.VALUE, attrValue);
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * entitySpecification
	 * 
	 * Passing entitySpecificationNode ,entitySpecification
	 * 
	 * @param entitySpecificationNode
	 *            is used to set the property
	 * @param entitySpecification
	 *            is used to hold the entity Specification Data
	 * 
	 * @return void
	 * 
	 */

	protected void entitySpecificationSetProperty(Node entitySpecificationNode,
			EntitySpecification entitySpecification) {
		try {
			entitySpecificationNode.setProperty(CatalogWriteConstants.ID, entitySpecification.getId());
			entitySpecificationNode.setProperty(CatalogWriteConstants.CATALOG_ID, entitySpecification.getCatalogId());
			entitySpecificationNode.setProperty(CatalogWriteConstants.NAME, entitySpecification.getName());
			entitySpecificationNode.setProperty(CatalogWriteConstants.TYPE, entitySpecification.getType());
			entitySpecificationNode.setProperty(CatalogWriteConstants.VALID_FROM,
					entitySpecification.getValidFrom().toGregorianCalendar());
			if (entitySpecification.getDescription() != null)
				entitySpecificationNode.setProperty(CatalogWriteConstants.DESCRIPTION,
						entitySpecification.getDescription());
			if (entitySpecification.getExternalId() != null)
				entitySpecificationNode.setProperty(CatalogWriteConstants.EXTERNAL_ID,
						entitySpecification.getExternalId());
			if (entitySpecification.getRefId() != null)
				entitySpecificationNode.setProperty(CatalogWriteConstants.REF_ID, entitySpecification.getRefId());
			if (entitySpecification.getValidTo() != null)
				entitySpecificationNode.setProperty(CatalogWriteConstants.VALID_TO,
						entitySpecification.getValidTo().toGregorianCalendar());
			// 8/6
			SpecificationVersion version = entitySpecification.getVersion();
			if (version != null) {
				Node specificationVersionNode = entitySpecificationNode.addNode(version.getName());
				entitySpecificationSetProperty(specificationVersionNode, version);
				specificationVersionNode.setProperty(CatalogWriteConstants.READABLE_VERSION,
						version.getReadableVersion());
				specificationVersionNode.setProperty(CatalogWriteConstants.STATUS, version.getStatus().value());

				// entitySpecification.getVersion().setStatus(specificationVersionNode.getProperty(CatalogWriteConstants.STATUS).getValue().);
				// 8/6
				List<StatusHistory> statusHistoryList = version.getHistory();

				if (!statusHistoryList.isEmpty()) {
					for (StatusHistory statusHistory : statusHistoryList) {
						statusHistory(specificationVersionNode, statusHistory);
					}
				}
			}
			XMLPathSpecification pathSpecification = entitySpecification.getPathSpecification();
			if (pathSpecification != null) {
				Node pathSpecificationNode = entitySpecificationNode.addNode(CatalogWriteConstants.PATH_SPECIFICATION);
				pathSpecificationNode.setProperty(CatalogWriteConstants.NODE_MULTIPLICITY,
						entitySpecification.getPathSpecification().getNodeMultiplicity());
				pathSpecificationNode.setProperty(CatalogWriteConstants.PATH,
						entitySpecification.getPathSpecification().getPath());
			}

			// Content Resource
			List<EntityContentResource> contentResourceList = entitySpecification.getContentResource();
			if (!contentResourceList.isEmpty()) {
				for (EntityContentResource entityContentResource : contentResourceList) {
					Node contentResourceNode = entitySpecificationNode.addNode(CatalogWriteConstants.CONTENT_RESOURCE);
					contentResourceNode.setProperty(CatalogWriteConstants.ID, entityContentResource.getId());
					contentResourceNode.setProperty(CatalogWriteConstants.KEY, entityContentResource.getKey());
					contentResourceNode.setProperty(CatalogWriteConstants.MIME_TYPE,
							entityContentResource.getMimeType());
					contentResourceNode.setProperty(CatalogWriteConstants.VALUE_TYPE,
							entityContentResource.getType().value());
					// 16/6
					Element element = (Element) entityContentResource.getValue();
					org.w3c.dom.Node node = element.getFirstChild();
					String attrValue = node.getNodeValue();
					contentResourceNode.setProperty(CatalogWriteConstants.VALUE, attrValue);
				}
			}
			List<Localization> localizedDescriptionList = entitySpecification.getLocalizedDescription();
			if (!localizedDescriptionList.isEmpty()) {
				// create localized description container node
				Node localizedDescNode = entitySpecificationNode
						.addNode(CatalogWriteConstants.LOCALIZED_DESCRIPTION_NODE);
				// set nodeType property to above created node
				localizedDescNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.LOCALIZED_DESCRIPTION);
				localizationSetProperty(localizedDescNode, localizedDescriptionList);

			}
			List<Localization> localizedNameList = entitySpecification.getLocalizedName();
			if (!localizedNameList.isEmpty()) {
				// create localized name container node
				Node localizedNameNode = entitySpecificationNode.addNode(CatalogWriteConstants.LOCALIZED_NAME_NODE);
				// set nodeType property to above created node
				localizedNameNode.setProperty(CatalogWriteConstants.TYPE, CatalogWriteConstants.LOCALIZED_NAME);
				localizationSetProperty(localizedNameNode, localizedNameList);
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the AmountNode and set the related properties
	 * .
	 * 
	 * Passing itemNode ,money ,nodeName
	 * 
	 * @param itemNode
	 *            object used to set the property .
	 * @param money
	 *            object is used to .
	 * 
	 * @return void
	 * 
	 */

	protected void createAmountNode(Node itemNode, Money money, String nodeName) {
		try {
			Node priceNode = itemNode.addNode(nodeName);
			priceNode.setProperty(CatalogWriteConstants.CURRENCY, money.getCurrency());
			priceNode.setProperty(CatalogWriteConstants.AMOUNT, money.getAmount());
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the applicabilitySpecNode and set the related
	 * properties
	 * 
	 * Passing priceItemSpecificationNode ,applicabilitySpecification
	 * 
	 * @param priceItemSpecificationNode
	 *            .
	 * @param applicabilitySpecification
	 *            object is used to hold the applicabilitySpecification data .
	 * 
	 * @return void
	 * 
	 */

	protected void applicabilitySpecNode(Node priceItemSpecificationNode,
			ApplicabilitySpecification applicabilitySpecification) {
		try {
			Node applicabilityspecificationNode = priceItemSpecificationNode
					.addNode(CatalogWriteConstants.APPLICABILITY_SPECIFICATION);
			// 31/05
			if (applicabilitySpecification.isApplicabilityEditable() != null) {
				applicabilityspecificationNode.setProperty(CatalogWriteConstants.IS_APPLICABILITY_EDITABLE,
						applicabilitySpecification.isApplicabilityEditable());
			}
			if (applicabilitySpecification.isOveruseAllowed() != null) {
				applicabilityspecificationNode.setProperty(CatalogWriteConstants.IS_OVERUSE_ALLOWED,
						applicabilitySpecification.isOveruseAllowed());
			}

			ApplicabilityRule applicabilityRule = applicabilitySpecification.getRule();
			if (applicabilityRule != null) {
				applicabilityRuleNode(applicabilityspecificationNode, applicabilityRule);
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the nodes and properties which are related to
	 * applicabilityRule .
	 * 
	 * Passing node ,applicabilityRule
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param applicabilityRule
	 *            object is used to hold the applicabilityRule data .
	 * 
	 * @return void
	 * 
	 */

	protected void applicabilityRuleNode(Node node, ApplicabilityRule applicabilityRule) {
		try {
			Node applicabilityRuleNode = node.addNode(applicabilityRule.getName());
			entitySpecificationSetProperty(applicabilityRuleNode, applicabilityRule);
			if (applicabilityRule.isRoamingOrInternational() != null) {
				applicabilityRuleNode.setProperty(CatalogWriteConstants.ROAMING_OR_INTERNATIONAL,
						applicabilityRule.isRoamingOrInternational());
			}
			if (applicabilityRule.isOutsideNetwork() != null) {
				applicabilityRuleNode.setProperty(CatalogWriteConstants.OUTSIDE_NETWORK,
						applicabilityRule.isOutsideNetwork());
			}
			ConditionExpression conditionExpressionOfApplicability = applicabilityRule.getConditionExpression();
			if (conditionExpressionOfApplicability != null) {
				createConditionExpressionNode(applicabilityRuleNode, conditionExpressionOfApplicability);
			}
			List<Country> countryList = applicabilityRule.getCountry();
			if (!countryList.isEmpty()) {
				for (Country country : countryList) {
					Node countryNode = applicabilityRuleNode.addNode(country.getName(),
							CatalogWriteConstants.NT_UNSTRUCTURED);
					entitySpecificationSetProperty(countryNode, country);
				}
			}
			List<Location> locationList = applicabilityRule.getLocation();
			if (!locationList.isEmpty()) {
				for (Location location : locationList) {
					Node locationNode = applicabilityRuleNode.addNode(location.getName());
					entitySpecificationSetProperty(locationNode, location);
				}
			}
			List<TimePeriod> timePeriodList = applicabilityRule.getTimePeriod();
			if (!timePeriodList.isEmpty()) {
				for (TimePeriod timePeriod : timePeriodList) {
					Node timePeriodNode = applicabilityRuleNode.addNode(CatalogWriteConstants.TIME_PERIOD);
					if (timePeriod.getDay().value() != null) {
						Node dayNode = timePeriodNode.addNode(CatalogWriteConstants.DAY_NODE);
						dayNode.setProperty(CatalogWriteConstants.DAY, timePeriod.getDay().value());
					}
					timePeriodNode.setProperty(CatalogWriteConstants.FROM_24_HOUR,
							timePeriod.getFrom24Hour() != null ? timePeriod.getFrom24Hour().longValue() : 0);
					timePeriodNode.setProperty(CatalogWriteConstants.TO_24_HOUR,
							timePeriod.getTo24Hour() != null ? timePeriod.getTo24Hour().longValue() : 0);
				}
			}
			List<String> phoneNumberList = applicabilityRule.getPhoneNumber();
			if (!phoneNumberList.isEmpty()) {
				for (String phoneNumber : phoneNumberList) {
					Node phoneNumberNode = applicabilityRuleNode.addNode(CatalogWriteConstants.PHONE_NUMBER_NODE);
					phoneNumberNode.setProperty(CatalogWriteConstants.PHONE_NUMBER, phoneNumber);
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	/**
	 * This method is used to add the ConditionExpressionNode and set the
	 * properties .
	 * 
	 * Passing node ,condition expression
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param conditionexpression
	 *            object is used to hold the conditionexpression data .
	 * 
	 * @return void
	 * 
	 */

	protected void createConditionExpressionNode(Node node, ConditionExpression conditionexpression) {
		try {
			if (conditionexpression != null) {
				Node conditionExpressionNode = node.addNode(conditionexpression.getName());
				entitySpecificationSetProperty(conditionExpressionNode, conditionexpression);
				List<ConditionExpressionItem> conditionExpressionItemList = conditionexpression.getItem();
				conditionExpressionItemList.size();
				if (!conditionExpressionItemList.isEmpty()) {
					for (ConditionExpressionItem conditionExpressionItem : conditionExpressionItemList) {
						// Node conditionExpressionItemNode =
						// conditionExpressionNode.addNode(CatalogWriteConstants.CONDITION_EXPRESSION_ITEM);
						// 19/6
						Node conditionExpressionItemNode = conditionExpressionNode
								.addNode(CatalogWriteConstants.CONDITION_EXPRESSION_ITEM + "_"
										+ conditionExpressionItem.getSequence().toString());
						conditionExpressionItemNode.setProperty(CatalogWriteConstants.SEQUENCE,
								conditionExpressionItem.getSequence().longValue());
						if (conditionExpressionItem instanceof ConditionOperator) {
							conditionExpressionItemNode.setProperty(CatalogWriteConstants.OPERATOR_TYPE,
									((ConditionOperator) conditionExpressionItem).getType().value());
						} else if (conditionExpressionItem instanceof ConditionBracket) {
							conditionExpressionItemNode.setProperty(CatalogWriteConstants.BRACKET_TYPE,
									((ConditionBracket) conditionExpressionItem).getType());
						} else if (conditionExpressionItem instanceof Condition) {
							Condition condition = (Condition) conditionExpressionItem;
							conditionExpressionItemNode.setProperty(CatalogWriteConstants.OPERATOR_TYPE,
									condition.getConditionOperator());
							ValueExpression leftValue = condition.getLeftValue();
							valueExpression(conditionExpressionItemNode, leftValue);
							ValueExpression rightValue = condition.getRightValue();
							valueExpression(conditionExpressionItemNode, rightValue);
						}
					}
				}
			}
		}

		catch (ValueFormatException valueEx) {
			log.info("Error:" + valueEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));

		} catch (LockException lockEx) {

			log.info("Error:" + lockEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));

		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));

		} catch (Exception ex) {

			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));

		}

	}

	// 8/6
	/**
	 * This method is used to add the statusHistoryNode and set the properties .
	 * 
	 * Passing node ,statusHistory
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param statusHistory
	 *            object is used to hold the StatusHistory data .
	 * 
	 * @return void
	 * 
	 */
	protected void statusHistory(Node node, StatusHistory statusHistory) {
		// Node statusHistoryNode;
		try {
			Node statusHistoryNode = node.addNode(CatalogWriteConstants.STATUS_HISTORY);
			statusHistoryNode.setProperty(CatalogWriteConstants.ID, statusHistory.getId());
			statusHistoryNode.setProperty(CatalogWriteConstants.REASON, statusHistory.getReason());
			statusHistoryNode.setProperty(CatalogWriteConstants.USER, statusHistory.getUser());
			statusHistoryNode.setProperty(CatalogWriteConstants.TIME_STAMP,
					statusHistory.getTimestamp().toGregorianCalendar());
			if (statusHistory.getNewStatus() != null) {
				statusHistoryNode.setProperty(CatalogWriteConstants.NEWSTATUS, statusHistory.getNewStatus().value());
				if (statusHistory.getOldStatus() != null) {
					statusHistoryNode.setProperty(CatalogWriteConstants.OLDSTATUS,
							statusHistory.getOldStatus().value());
				}
			}
		} catch (RepositoryException repoEx) {
			log.info("Error:" + repoEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		} catch (Exception ex) {
			log.info("Error:" + ex.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));
		}
	}

	// 19/6
	/**
	 * This method is used to add the valueExpressionNode and set the properties
	 * .
	 * 
	 * Passing node ,valueExpression
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param valueExpression
	 *            object is used to hold the ValueExpression data .
	 * 
	 * @return void
	 * 
	 */
	protected void valueExpression(Node itemNode, ValueExpression valueExpression) {

		Node valueExpressionNode;
		try {
			valueExpressionNode = itemNode.addNode(valueExpression.getName(), CatalogWriteConstants.NT_UNSTRUCTURED);
			entitySpecificationSetProperty(valueExpressionNode, valueExpression);
			List<ValueExpressionItem> valueExpressionItemList = valueExpression.getItem();
			if (!valueExpressionItemList.isEmpty()) {
				for (ValueExpressionItem valueExpressionItem : valueExpressionItemList) {
					Node valueExpressionItemNode = valueExpressionNode.addNode(
							CatalogWriteConstants.VALUE_EXPRESSION_ITEM + "_" + valueExpressionItem.getSequence());
					valueExpressionItemNode.setProperty(CatalogWriteConstants.SEQUENCE,
							valueExpressionItem.getSequence().intValue());
					if (valueExpressionItem instanceof ValueCRM) {
						valueExpressionItemNode.setProperty(CatalogWriteConstants.VALUE_GENERATOR_NAME,
								((ValueCRM) valueExpressionItem).getValueGeneratorName());
					} else if (valueExpressionItem instanceof ValueCharacteristic) {
						CharacteristicSpecification characteristicSpec = ((ValueCharacteristic) valueExpressionItem)
								.getCharacteristicSpecification();
						characteristicSpecification(valueExpressionItemNode, characteristicSpec);
					} else if (valueExpressionItem instanceof ValueFixed) {
						BigDecimal decimalValue = ((ValueFixed) valueExpressionItem).getDecimalValue();
						if (decimalValue != null) {
							valueExpressionItemNode.setProperty(CatalogWriteConstants.DECIMAL_VALUE,
									decimalValue.intValue());
						}
						BigInteger integerValue = ((ValueFixed) valueExpressionItem).getIntegerValue();
						if (integerValue != null) {
							valueExpressionItemNode.setProperty(CatalogWriteConstants.INTEGER_VALUE,
									integerValue.intValue());
						}
					} else if (valueExpressionItem instanceof ValueBracket) {
						valueExpressionItemNode.setProperty(CatalogWriteConstants.BRACKET_TYPE,
								((ValueBracket) valueExpressionItem).getType());
					} else if (valueExpressionItem instanceof ValueOperator) {
						valueExpressionItemNode.setProperty(CatalogWriteConstants.OPERATOR_TYPE,
								((ValueOperator) valueExpressionItem).getType());
					}
				}
			}
		} catch (RepositoryException repositoryEx) {
			log.info("Error:" + repositoryEx.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}

	}

	/**
	 * This method is used to add the valueExpressionNode and set the properties
	 * .
	 * 
	 * Passing node ,List of Localization
	 * 
	 * @param node
	 *            object used to set the property .
	 * @param localization
	 *            object is used to iterate over the list of Localization
	 *            objects and hold the localization data .
	 * 
	 * @return void
	 * 
	 */
	protected void localizationSetProperty(Node node, List<Localization> localization) {
		for (Localization localizedDescription : localization) {
			try {
				Node localizedDescChildNode = node.addNode(localizedDescription.getLocale());
				localizedDescChildNode.setProperty(CatalogWriteConstants.ID, localizedDescription.getId());
				localizedDescChildNode.setProperty(CatalogWriteConstants.LOCALE, localizedDescription.getLocale());
				localizedDescChildNode.setProperty(CatalogWriteConstants.LOCALIZED_VALUE,
						localizedDescription.getLocalizedValue());
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
