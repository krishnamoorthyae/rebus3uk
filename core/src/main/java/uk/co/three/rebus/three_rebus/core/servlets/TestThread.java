/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.servlets;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

/**
 * @author DD00493288
 *
 */
public class TestThread extends Thread {
	
	private TestModel testModel = null;
	
	private long timeToSleep = 0;

	/**
	 * @param testModel
	 */
	public TestThread(TestModel testModel, long timeToSleep) {
		this.testModel = testModel;
		this.timeToSleep = timeToSleep;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		testModel.setStartTime(System.currentTimeMillis());
		
		try {
			
				HttpGet post = new HttpGet("http://localhost:4502/bin/simple");
							org.apache.http.impl.client.DefaultHttpClient client = new org.apache.http.impl.client.DefaultHttpClient();
							org.apache.http.HttpResponse response = null;

							response = client.execute(post);
							
							
		} catch (Exception e) {
			// do nothing
		}
		
		testModel.setEndTime(System.currentTimeMillis());
		
	}

}
