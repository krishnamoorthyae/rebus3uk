package uk.co.three.rebus.three_rebus.core.productmodel.response;

import java.io.Serializable;

import uk.co.three.rebus.three_rebus.core.response.BaseResponse;
import epc.h3g.com.OfferingsDetails;

public class CatalogResponse extends BaseResponse implements Serializable {
	private OfferingsDetails offeringsDetailsObj = null;

	public OfferingsDetails getOfferingsDetailsObj() {
		return offeringsDetailsObj;
	}

	public void setOfferingsDetailsObj(OfferingsDetails offeringsDetailsObj) {
		this.offeringsDetailsObj = offeringsDetailsObj;
	}

}
