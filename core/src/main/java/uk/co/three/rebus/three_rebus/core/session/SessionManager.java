/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.session;

import uk.co.three.rebus.three_rebus.core.productmodel.service.SessionHandlerService;

/**
 * This class manages session - this is a singleton
 * @author DD00493288
 *
 */
public class SessionManager {

	private static SessionManager s_instance = null;
	
	private SessionHandlerService sessionHandler = null;
	
	/**
	 * Private constructor of the class
	 */
	private SessionManager(){
		sessionHandler = new SessionHandlerImpl();
	}
	
	/**
	 * public method to get the instance of this class
	 * @return
	 */
	public static SessionManager getInstance(){
		if(s_instance == null){
			s_instance = new SessionManager();
		}
		return s_instance;
	}
	
	/**
	 * Method to return the sessionhandler
	 * @return
	 */
	public SessionHandlerService getSessionHandler(){
		return sessionHandler;
	}
	
}
