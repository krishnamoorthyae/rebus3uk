package uk.co.three.rebus.three_rebus.core.productmodel.plain;

import java.math.BigInteger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Component;

import uk.co.three.rebus.three_rebus.core.productmodel.request.GetProductDetailsRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetProductDetailsResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.service.SessionHandlerService;
import uk.co.three.rebus.three_rebus.core.session.SessionManager;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;
import epc.h3g.com.AddressLocalization;
import epc.h3g.com.AvailabilityRule;
import epc.h3g.com.CharacteristicGroupSpecification;
import epc.h3g.com.CharacteristicSpecification;
import epc.h3g.com.CharacteristicValue;
import epc.h3g.com.CharacteristicValueType;
import epc.h3g.com.CompatibilityRule;
import epc.h3g.com.ConditionExpression;
import epc.h3g.com.ConditionExpressionItem;
import epc.h3g.com.CustomerSegment;
import epc.h3g.com.EligibilityRule;
import epc.h3g.com.EntityContentResource;
import epc.h3g.com.EntitySpecification;
import epc.h3g.com.Localization;
import epc.h3g.com.Location;
import epc.h3g.com.MarketSegment;
import epc.h3g.com.PaymentMethod;
import epc.h3g.com.ProductOfferingSpecification;
import epc.h3g.com.SellingChannel;
import epc.h3g.com.Specification;
import epc.h3g.com.SpecificationStatus;
import epc.h3g.com.StatusHistory;

@Component(label = "Title", description = "Description.")
public class ProductDetailsProvider {

	
   /*public  void getProduct (Session session, String Category,String SubCategory,String Product) {
		
	   GetProductDetailsRequest request = new GetProductDetailsRequest();
		
		StringBuilder productOffering = new StringBuilder("SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/three_rebus/products/catalogOne/");
		productOffering.append(Category).append("/").append(SubCategory).append("]) and CONTAINS(s.*, '").append(Product).append("') AND nodeType='PRODUCT_OFFERING'");
		
		getProductDetail(session,request,productOffering );
	}*/
	

	
	public GetProductDetailsResponse getProductDetail(GetProductDetailsRequest request) {
		// initialize the response
		GetProductDetailsResponse response = new GetProductDetailsResponse();
		ProductOfferingSpecification productOfferingSpecification = new ProductOfferingSpecification();
		
		// get the session to get AEM resources
		SessionHandlerService sessionHandler = SessionManager.getInstance().getSessionHandler();
		
		
		// get the path from request
		String productPath = request.getProductPath();
		// form the query
		StringBuilder productOffering = new StringBuilder("SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/three_rebus/products/catalogOne/");
		productOffering.append(productPath).append("]) AND nodeType='PRODUCT_OFFERING'");
		
		
		try {
			Session session = sessionHandler.getSession();
			QueryManager queryManager = session.getWorkspace().getQueryManager();
			Query query = queryManager.createQuery(productOffering.toString(),
					Query.JCR_SQL2);
			QueryResult result = query.execute();

			NodeIterator nodeIterator = result.getNodes();
			while (nodeIterator.hasNext()) {
				
	             getAllProperties(productOfferingSpecification, nodeIterator.nextNode());
	             response.setProduct(productOfferingSpecification);
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
		
		return response;
	}

	private ProductOfferingSpecification getAllProperties(ProductOfferingSpecification productOfferingSpecification, Node node) {
	
		
		try {
			if (node.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.PRODUCT_OFFERING)) {
			readProductOfferNode(productOfferingSpecification, node);

			NodeIterator nodeitertor = node.getNodes();
			while (nodeitertor.hasNext()) {
				Node chilNode = nodeitertor.nextNode();
				//getAllProperties(productOfferingSpecification,chilNode);

				if(chilNode.hasProperty(RebusUtils.TYPE)){
				 if(chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.ACCOUNT_TYPE)){
					 productOfferingSpecification = (ProductOfferingSpecification) entitySpecificationGetProperty(node, productOfferingSpecification.getAccountType());
					 
			        }
				 
				  if (chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.PAYMENT_METHOD)) {
					  PaymentMethod paymentMethod = new PaymentMethod();
					  paymentMethod =  (PaymentMethod) entitySpecificationGetProperty(chilNode, paymentMethod);
					  
					 productOfferingSpecification.getPaymentMethod().add(paymentMethod);
					 
					
				}
				 
				  if (chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.AVAILABILITY_RULE)) {
					  
					  AvailabilityRule availabilityRule = new AvailabilityRule();
					  availabilityRule = (AvailabilityRule) entitySpecificationGetProperty(chilNode, availabilityRule);
					  productOfferingSpecification.getAvailabilityRule().add(availabilityRule);
					  NodeIterator availabilityruleIt = node.getNodes();
					  while (availabilityruleIt.hasNext()) {
					  Node availabilityNode = availabilityruleIt.nextNode(); 
					  if (availabilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CONDITION_EXPRESSION)) {
						   readConditionExpression(availabilityRule.getConditionExpression(),availabilityNode);
						 
						
					}if (availabilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.LOCATION)) {
						Location location = new Location();
						readLocation(location,availabilityNode);
						location = (Location) entitySpecificationGetProperty(availabilityNode, location);
						availabilityRule.getLocation().add(location);
						 
						
						  
					  }
					  
					 
					 					
				}
				 
				  if (chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.ELIGIBILITY_RULE)) {
					 
					  EligibilityRule eligibilityRule = new EligibilityRule();
					  eligibilityRule =  (EligibilityRule) entitySpecificationGetProperty(chilNode, eligibilityRule);
					  productOfferingSpecification.getEligibilityRule().add(eligibilityRule);
					  if (chilNode.hasNodes()) {
						  NodeIterator eligibilityRuleIt = chilNode.getNodes();
						  while (eligibilityRuleIt.hasNext()) {

								Node eligibilityNode = eligibilityRuleIt.nextNode();
								if (eligibilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CONDITION_EXPRESSION)){
									readConditionExpression(eligibilityRule.getConditionExpression(),eligibilityNode);
								} if (eligibilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.MARKET_SEGMENT)) {
									MarketSegment marketSegment = new MarketSegment();
									marketSegment	= (MarketSegment) entitySpecificationGetProperty(eligibilityNode, marketSegment); 
									eligibilityRule.getMarketSegment().add(marketSegment);
									
                                }
								 if (eligibilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.SELLING_CHANNEL)) {
									 
									 SellingChannel sellingChannel = new SellingChannel();
									 sellingChannel = (SellingChannel) entitySpecificationGetProperty(eligibilityNode, sellingChannel);
									 eligibilityRule.getSellingChannel().add(sellingChannel);
									 
									
									
									
								} if (eligibilityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CUSTOMER_SEGMENT)) {
									CustomerSegment customerSegment = new CustomerSegment();
									
									if (eligibilityNode.hasNodes()) {
										NodeIterator eligibilityIt = eligibilityNode.getNodes();
										while (eligibilityIt.hasNext()) {
											Node marketsegmentNode = eligibilityIt.nextNode();
											MarketSegment marketSegment = new MarketSegment();
											marketSegment = (MarketSegment) entitySpecificationGetProperty(marketsegmentNode, marketSegment);
											customerSegment.getMarketSegment().add(marketSegment);
											
										}
										
										
									}
									customerSegment = (CustomerSegment) entitySpecificationGetProperty(eligibilityNode, customerSegment);
									eligibilityRule.getCustomerSegment().add(customerSegment);
									
								}
								
							
							
							
						}
					  }
						
					}
					  
					
					
				}
				 
				 if (chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.COMPATIBILITY_RULE)) {
					 CompatibilityRule compatibilityRule=new CompatibilityRule();
					 compatibilityRule = (CompatibilityRule) entitySpecificationGetProperty(chilNode, compatibilityRule);
					 productOfferingSpecification.getCompatibilityRule().add(compatibilityRule);
					 
					 for(CompatibilityRule compatibilityRule1 : productOfferingSpecification.getCompatibilityRule()){
						 productOfferingSpecification = (ProductOfferingSpecification) entitySpecificationGetProperty(node, compatibilityRule);
						 
						 
						 
						 
					 }
					
				}
				 
				 
				 
				 
				 
				  if(chilNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.PRODUCT_SPECIFICATION)){
					 // read product specification
					  // readProductSpecification(productOfferingSpecification,node);
					   // read characetiristic specification
					   //readCharacteristicSpecification(productOfferingSpecification,node);
					     
					   


			        }/*else if (node.getProperty(RebusUtils.NODE_TYPE).getValue().getString().equalsIgnoreCase("ELIGIBILITY_RULE")) {
			        	
			        	//read eligibility rule
			        	readEligibilityRule(productOfferingSpecification, node);
						
					}*/
				
			}

			}
		
			}
		
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		return productOfferingSpecification;
	}



	private Location readLocation(Location location, Node locationNode) {
		
		
		try {
			
			if (locationNode.hasNode(RebusUtils.ADDRESS)) {
				Node addressNode = locationNode.getNode(RebusUtils.ADDRESS);
				  
					  if(addressNode.hasProperty(RebusUtils.APARTMENT_NUMBER)){
						  location.getAddress().setApartmentNumber(addressNode.getProperty(RebusUtils.APARTMENT_NUMBER).getValue().getString());  
					  }
					  
					  location.getAddress().setCity(addressNode.getProperty(RebusUtils.CITY).getValue().getString());
					  if(addressNode.hasProperty(RebusUtils.COUNTRY)){
						  location.getAddress().setCountry(addressNode.getProperty(RebusUtils.COUNTRY).getValue().getString());  
					  } if(addressNode.hasProperty(RebusUtils.LOCALITY)){
						  location.getAddress().setLocality(addressNode.getProperty(RebusUtils.LOCALITY).getValue().getString());  
					  } 
						  location.getAddress().setPostCode(addressNode.getProperty(RebusUtils.POST_CODE).getValue().getString());  
						  if(addressNode.hasProperty(RebusUtils.POSTAL_ADDRESS_CODE)){
							  location.getAddress().setPostalAddressCode(addressNode.getProperty(RebusUtils.LOCALITY).getValue().getString());  
						  } 
						  if(addressNode.hasProperty(RebusUtils.STATE_OR_PROVINCE)){
							  location.getAddress().setStateOrProvince(addressNode.getProperty(RebusUtils.STATE_OR_PROVINCE).getValue().getString());  
						  } if(addressNode.hasProperty(RebusUtils.STREET_NAME)){
							  location.getAddress().setStreetName(addressNode.getProperty(RebusUtils.STREET_NAME).getValue().getString());  
						  }if(addressNode.hasProperty(RebusUtils.STREET_NUMBER_FIRST)){
							  location.getAddress().setStreetNumberFirst(addressNode.getProperty(RebusUtils.STREET_NUMBER_FIRST).getValue().getString());  
						  }if(addressNode.hasProperty(RebusUtils.STREET_NUMBER_LAST)){
							  location.getAddress().setStreetNumberLast(addressNode.getProperty(RebusUtils.STREET_NUMBER_LAST).getValue().getString());  
						  }
					    if(addressNode.hasNode(RebusUtils.ADDRESS_LOCALIZATION)){
					    	AddressLocalization localization = location.getAddress().getLocalization();
					    	
					    	Node addressLocalizationNode = addressNode.getNode(RebusUtils.ADDRESS_LOCALIZATION);
					    	if (addressLocalizationNode.hasNodes()) {
					    	 NodeIterator addressLocalizationIt = addressLocalizationNode.getNodes();
					    	  while (addressLocalizationIt.hasNext()) {
								Node addressLocalizNode = addressLocalizationIt.nextNode();
								if (addressLocalizNode.hasNode(RebusUtils.LOCALIZED_CITY)) {
									Localization localizedCityName = new Localization();
									localizedCityName.setId(addressLocalizNode.getProperty(RebusUtils.ID).getValue().getLong());
									localizedCityName.setLocale(addressLocalizNode.getProperty(RebusUtils.LOCALE).getValue().getString());
									localizedCityName.setLocalizedValue(addressLocalizNode.getProperty(RebusUtils.LOCALIZED_VALUE).getValue().getString());
									localization.getLocalizedCityName().add(localizedCityName);
									
									
									
								}
								 if (addressLocalizNode.hasNode(RebusUtils.LOCALIZED_STATE_OR_PROVINCE)) {
									
									 Localization localizedStateOrProvinceName = new Localization();
									 localizedStateOrProvinceName.setId(addressLocalizNode.getProperty(RebusUtils.ID).getValue().getLong());
									 localizedStateOrProvinceName.setLocale(addressLocalizNode.getProperty(RebusUtils.LOCALE).getValue().getString());
									 localizedStateOrProvinceName.setLocalizedValue(addressLocalizNode.getProperty(RebusUtils.LOCALIZED_VALUE).getValue().getString());
									 localization.getLocalizedStateOrProvinceName().add(localizedStateOrProvinceName);
									 
									
								} if (addressLocalizNode.hasNode(RebusUtils.LOCALIZED_STREET)) {
									
									 Localization localizedStreetName = new Localization();
									 localizedStreetName.setId(addressLocalizNode.getProperty(RebusUtils.ID).getValue().getLong());
									 localizedStreetName.setLocale(addressLocalizNode.getProperty(RebusUtils.LOCALE).getValue().getString());
									 localizedStreetName.setLocalizedValue(addressLocalizNode.getProperty(RebusUtils.LOCALIZED_VALUE).getValue().getString());
									 localization.getLocalizedStateOrProvinceName().add(localizedStreetName);
									
									
									
								}
								
							}
								
							}
					    	
					    }
				
			} if (locationNode.hasNode(RebusUtils.SUPPLIERPARTNER)) {
				Node supplierNode = locationNode.getNode(RebusUtils.SUPPLIERPARTNER);
				location.getSupplierPartner().setId(supplierNode.getProperty(RebusUtils.ID).getValue().getLong());
				if(supplierNode.hasProperty(RebusUtils.NAME)){
					location.getSupplierPartner().setName(supplierNode.getProperty(RebusUtils.NAME).getValue().getString());
				}location.getSupplierPartner().setSupplierId(supplierNode.getProperty(RebusUtils.SUPPLIER_ID).getValue().getString());
				if(supplierNode.hasProperty(RebusUtils.TAX_NUMBER)){
					location.getSupplierPartner().setTaxNumber(supplierNode.getProperty(RebusUtils.TAX_NUMBER).getValue().getString());
				}if(supplierNode.hasProperty(RebusUtils.VAT_ID)){
					location.getSupplierPartner().setName(supplierNode.getProperty(RebusUtils.VAT_ID).getValue().getString());
				}
				
			}
			
			
			
			
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return location;
		
	}


	private ConditionExpression readConditionExpression(ConditionExpression conditionExpression,
			Node conditionExpressionNode) {
		
		
		
		try {
			
			conditionExpression =    (ConditionExpression) entitySpecificationGetProperty(conditionExpressionNode, conditionExpression);
			
				NodeIterator conditionExpressionit = conditionExpressionNode.getNodes();
				while (conditionExpressionit.hasNext()) {
					Node conditionExpressionItemNode = conditionExpressionit.nextNode();
					ConditionExpressionItem conditionExpressionItem = new ConditionExpressionItem();
					conditionExpressionItem.setSequence(BigInteger.valueOf( conditionExpressionItemNode.getProperty(RebusUtils.SEQUENCE).getValue().getLong()));
					conditionExpression.getItem().add(conditionExpressionItem);
					
					
				}
			
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conditionExpression;
	}

	
	//read ProductofferingSpecification

	private ProductOfferingSpecification readProductOfferNode(
			ProductOfferingSpecification productOfferingSpecification, Node node) {
		try {
			   
			 productOfferingSpecification	= (ProductOfferingSpecification) entitySpecificationGetProperty(node, productOfferingSpecification);
			
			
			productOfferingSpecification.setActivation(node.hasProperty(RebusUtils.IS_ACTIVATED)? node.getProperty(RebusUtils.IS_ACTIVATED).getValue().getBoolean():false );
			productOfferingSpecification.setAddon(node.hasProperty(RebusUtils.IS_ADDON)? node.getProperty(RebusUtils.IS_ADDON).getValue().getBoolean():false );
			productOfferingSpecification.setCommercial(node.hasProperty(RebusUtils.IS_ADDON)? node.getProperty(RebusUtils.IS_ADDON).getValue().getBoolean():false );
			productOfferingSpecification.setComplex(node.hasProperty(RebusUtils.IS_COMPLEX)? node.getProperty(RebusUtils.IS_COMPLEX).getValue().getBoolean():false );
			productOfferingSpecification.setShareable(node.hasProperty(RebusUtils.IS_SHAREABLE)? node.getProperty(RebusUtils.IS_SHAREABLE).getValue().getBoolean():false );
			specificationGetProperty(productOfferingSpecification, node);	
			
				
			
		} catch (ValueFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productOfferingSpecification;
		
	}


 private Specification specificationGetProperty(Specification specification, Node node) {
	 try {
		specification.setStoreInventory(node.getProperty(RebusUtils.STORE_INVENTORY).getValue().getBoolean());
		specification.setStatus(SpecificationStatus.valueOf(node.getProperty(RebusUtils.STATUS).getValue().getString()));
		 NodeIterator specificationNodeIt = node.getNodes();
		 
		 while (specificationNodeIt.hasNext()) {
			Node specificationNode   =  specificationNodeIt.nextNode();
			
			if (specificationNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CHARACTERISTIC_GROUP_SPECIFICATION)) {
				 
				 for (CharacteristicGroupSpecification characteristicGroupSpecification : specification.getCharacteristicGroupSpecification()) {
					 
					entitySpecificationGetProperty(specificationNode, characteristicGroupSpecification);
					characteristicGroupSpecification.setMinMultiplicity(BigInteger.valueOf(specificationNode.getProperty(RebusUtils.MIN_MULTIPICITY).getValue().getLong()));
					if (specificationNode.hasProperty(RebusUtils.MAX_MULTIPICITY)) {
						characteristicGroupSpecification.setMaxMultiplicity(BigInteger.valueOf(specificationNode.getProperty(RebusUtils.MAX_MULTIPICITY).getValue().getLong()));	
					}
					
					if (specificationNode.hasNodes()) {
						
						NodeIterator characteristicGroupSpecIt = specificationNode.getNodes();
						while (characteristicGroupSpecIt.hasNext()) {
							Node characteristicGroupSpecNode =  characteristicGroupSpecIt.nextNode();
							if (characteristicGroupSpecNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CHARACTERISTIC)) {
								for (CharacteristicSpecification characteristic : characteristicGroupSpecification.getCharacteristicSpecifications()) {
									
									characteristicgetSpecification(characteristic,characteristicGroupSpecNode);
									
								}
							}if (characteristicGroupSpecNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.COMPONENT_CHARACTERISTIC_GROUP_SPECIFICATION)) {
								
								/*for (iterable_type iterable_element : iterable) {
									
								}*/
								
							}
					}
				}
			}
		}
			
			if(specificationNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.CHARACTERISTIC)){
				for (CharacteristicSpecification characteristic : specification.getCharacteristicSpecification()) {
					
					characteristicgetSpecification(characteristic,specificationNode);
					
				}
				}if (specificationNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.STATUS_HISTORY)) {
					for (StatusHistory history : specification.getHistory()) {
						history.setId(specificationNode.getProperty(RebusUtils.ID).getValue().getLong());
						history.setReason(specificationNode.getProperty(RebusUtils.REASON).getValue().getString());
						history.setUser(specificationNode.getProperty(RebusUtils.USER).getValue().getString());
						history.setId(specificationNode.getProperty(RebusUtils.ID).getValue().getLong());
						history.setId(specificationNode.getProperty(RebusUtils.ID).getValue().getLong());
						history.setNewStatus(SpecificationStatus.valueOf(specificationNode.getProperty(RebusUtils.NEWSTATUS).getValue().getString()));
						history.setOldStatus(SpecificationStatus.valueOf(specificationNode.getProperty(RebusUtils.OLDSTATUS).getValue().getString()));
						//history.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar().setTime(specificationNode.getProperty(RebusUtils.TIME_STAMP).getValue().getDate())));
						
					}
					
				}
			
		 }
	 }catch (RepositoryException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	 

	return specification;
		
}
	 
	 
	 
	 
	            
	 
	 

		


//read CharacteristicSpecification
 
private CharacteristicSpecification characteristicgetSpecification(
		CharacteristicSpecification characteristic,
		Node characteristicGroupSpecNode) {
	
	entitySpecificationGetProperty(characteristicGroupSpecNode, characteristic);
	
	
	try {
		/* Mandatory Properties */
		characteristic.setCommercial(characteristicGroupSpecNode.getProperty(RebusUtils.IS_COMMERICAL).getValue().getBoolean());
		characteristic.setEditable(characteristicGroupSpecNode.getProperty(RebusUtils.IS_EDITABLE).getValue().getBoolean());
		characteristic.setMandatory(characteristicGroupSpecNode.getProperty(RebusUtils.IS_MANDATORY).getValue().getBoolean());
		characteristic.setPrintable(characteristicGroupSpecNode.getProperty(RebusUtils.IS_PRINTABLE).getValue().getBoolean());
		characteristic.setVisible(characteristicGroupSpecNode.getProperty(RebusUtils.IS_VISIBLE).getValue().getBoolean());
		characteristic.setMinMultiplicity(BigInteger.valueOf(characteristicGroupSpecNode.getProperty(RebusUtils.IS_COMMERICAL).getValue().getLong()));
		characteristic.setValueType(CharacteristicValueType.valueOf(characteristicGroupSpecNode.getProperty(RebusUtils.VALUE_TYPE).getValue().getString()));
		
		/*
		 * Optinal Properties*/
		if (characteristicGroupSpecNode.hasProperty(RebusUtils.SCALE)) {
			characteristic.setScale(BigInteger.valueOf(characteristicGroupSpecNode.getProperty(RebusUtils.SCALE).getValue().getLong()));
		}if (characteristicGroupSpecNode.hasProperty(RebusUtils.MAX_SIZE)) {
			characteristic.setMaxSize(BigInteger.valueOf(characteristicGroupSpecNode.getProperty(RebusUtils.MAX_SIZE).getValue().getLong()));
		}if (characteristicGroupSpecNode.hasProperty(RebusUtils.SCALE)) {
			characteristic.setMaxMultiplicity(BigInteger.valueOf(characteristicGroupSpecNode.getProperty(RebusUtils.MAX_MULTIPICITY).getValue().getLong()));
		}
		      if (characteristicGroupSpecNode.hasNodes()) {
		    	  NodeIterator characteristicSpectIt = characteristicGroupSpecNode.getNodes();
				  while (characteristicSpectIt.hasNext()) {
					Node characteristicSpectNode = characteristicSpectIt.nextNode();
					
					if (characteristicSpectNode.hasNode(RebusUtils.ALLOWED_VALUE)) {
						Node allowedValueNode = characteristicSpectNode.getNode(RebusUtils.ALLOWED_VALUE);
						for (CharacteristicValue allowedValue : characteristic.getAllowedValue()) {
							allowedValue.setValue(allowedValueNode.getProperty(RebusUtils.DEFAULT_VALUE).getValue().getString());	
							
							
						}
						
					}if (characteristicSpectNode.hasNode(RebusUtils.DEFAULT_VALUE)) {
						
						Node defaultValueNode = characteristicSpectNode.getNode(RebusUtils.DEFAULT_VALUE);
						readCharacteristicValue(characteristic,defaultValueNode);
						
						
						
						
					}if (characteristicSpectNode.hasNode(RebusUtils.MAX_VALUE)) {
						
						Node maxValueNode = characteristicSpectNode.getNode(RebusUtils.MAX_VALUE);
						readCharacteristicValue(characteristic,maxValueNode);
						
					}if (characteristicSpectNode.hasNode(RebusUtils.MIN_VALUE)) {
						Node minValueNode = characteristicSpectNode.getNode(RebusUtils.MAX_VALUE);
						readCharacteristicValue(characteristic,minValueNode);
						
					}
					
					
				}
			}
		
	} catch (ValueFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (PathNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (RepositoryException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return characteristic;
}

private CharacteristicSpecification readCharacteristicValue(
		CharacteristicSpecification characteristic, Node characteristicValueNode) {
	try {
		characteristic.getDefaultValue().setValueType(CharacteristicValueType.valueOf(characteristicValueNode.getProperty(RebusUtils.VALUE_TYPE).getValue().getString()));
		characteristic.getDefaultValue().setValue(characteristicValueNode.getProperty(RebusUtils.VALUE).getValue().getString());
	} catch (ValueFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalStateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (PathNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (RepositoryException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return characteristic;
	
	
}


//read EntitySpecification common method
private EntitySpecification entitySpecificationGetProperty(Node entitySpecificationNode,
			EntitySpecification entitySpecification) {
		 try {
			 
			   entitySpecification.setId(entitySpecificationNode.getProperty(RebusUtils.ID).getValue().getLong());

				entitySpecification.setCatalogId(entitySpecificationNode.getProperty(RebusUtils.CATALOG_ID).getValue().getString());

				entitySpecification.setName(entitySpecificationNode.getProperty(RebusUtils.NAME).getValue().getString());
				
				
				entitySpecificationNode.setProperty(RebusUtils.VALID_FROM,
						entitySpecification.getValidFrom().toGregorianCalendar());
				if (entitySpecificationNode.hasProperty(RebusUtils.DESCRIPTION)){
					entitySpecification.setDescription(entitySpecificationNode.getProperty(RebusUtils.DESCRIPTION).getValue().getString());
					}
				if (entitySpecificationNode.hasProperty(RebusUtils.EXTERNAL_ID)){
					entitySpecification.setExternalId(entitySpecificationNode.getProperty(RebusUtils.EXTERNAL_ID).getValue().getString());
					
					}
				if (entitySpecificationNode.hasProperty(RebusUtils.REF_ID)){
					entitySpecification.setRefId(entitySpecificationNode.getProperty(RebusUtils.REF_ID).getValue().getString());
					
					}
				
				if (entitySpecification.getValidTo() != null)
					entitySpecificationNode.setProperty(RebusUtils.VALID_TO,
							entitySpecification.getValidTo().toGregorianCalendar());
				 
				NodeIterator entitySpecificationNodeIterator = entitySpecificationNode.getNodes();
				while (entitySpecificationNodeIterator.hasNext()) {
					
					Node entityNode = entitySpecificationNodeIterator.nextNode();
					if (entityNode.getProperty(RebusUtils.TYPE).getValue().getString().equalsIgnoreCase(RebusUtils.VERSION)) {
						
						entitySpecification.getVersion().setReadableVersion(entityNode.getProperty(RebusUtils.READABLE_VERSION).getValue().getString());
						//entitySpecification.getVersion().setStatus();
						     NodeIterator versionIt = entityNode.getNodes();
						     while (versionIt.hasNext()) {
								Node statusHistoryNode = versionIt.nextNode();
								for (StatusHistory statusHistory : entitySpecification.getVersion().getHistory()) {
									statusHistory.setId(statusHistoryNode.getProperty(RebusUtils.ID).getValue().getLong());
									statusHistory.setReason(statusHistoryNode.getProperty(RebusUtils.REASON).getValue().getString());
									statusHistory.setUser(statusHistoryNode.getProperty(RebusUtils.USER).getValue().getString());
									//statusHistory.setNewStatus(value);
									//statusHistory.setOldStatus(value);Status(value);
									//statusHistory.setTimestamp(value);
								}
								
							}
						
						
					}
					 if (entityNode.getName().equalsIgnoreCase(RebusUtils.PATH_SPECIFICATION)) {
						
						entitySpecification.getPathSpecification().setPath(entityNode.getProperty(RebusUtils.PATH).getValue().getString());
						entitySpecification.getPathSpecification().setNodeMultiplicity(entityNode.getProperty(RebusUtils.NODE_MULTIPLICITY).getValue().getString());
						
					}
					 if (entityNode.getName().equalsIgnoreCase(RebusUtils.CONTENT_RESOURCE)) {
						for (EntityContentResource entityContentResource : entitySpecification.getContentResource()) {
							entityContentResource.setId(entityNode.getProperty(RebusUtils.ID).getValue().getLong());
							entityContentResource.setKey(entityNode.getProperty(RebusUtils.KEY).getValue().getString());
							entityContentResource.setMimeType(entityNode.getProperty(RebusUtils.MIME_TYPE).getValue().getString());
							entityContentResource.setValue(entityNode.getProperty(RebusUtils.VALUE).getValue().getString());
							//entityContentResource.setType(entityNode.getProperty(RebusUtils.TYPE).getValue();
						}
						
					} if (entityNode.getName().equalsIgnoreCase(RebusUtils.LOCALIZED_DESCRIPTION)) {
						for (Localization localizedDescription : entitySpecification.getLocalizedDescription()) {
							localizedDescription.setId(entityNode.getProperty(RebusUtils.ID).getValue().getLong());
							localizedDescription.setLocale(entityNode.getProperty(RebusUtils.LOCALE).getValue().getString());
							localizedDescription.setLocalizedValue(entityNode.getProperty(RebusUtils.LOCALIZED_VALUE).getValue().getString());
						}
						
					} if (entityNode.getName().equalsIgnoreCase(RebusUtils.LOCALIZED_NAME)) {
						for (Localization localizedName : entitySpecification.getLocalizedName()) {
							localizedName.setId(entityNode.getProperty(RebusUtils.ID).getValue().getLong());
							localizedName.setLocale(entityNode.getProperty(RebusUtils.LOCALE).getValue().getString());
							localizedName.setLocalizedValue(entityNode.getProperty(RebusUtils.LOCALIZED_VALUE).getValue().getString());
							
						}}
					
					
				}
				
				
			}

			catch (ValueFormatException valueEx) {
				/*log.info("Error:" + valueEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.VALUE_FORMATE_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.VALUE_FORMATE_MESSAGE));*/

			} catch (LockException lockEx) {

				/*log.info("Error:" + lockEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.LOCK_EXEPTION,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.LOCK_EXEPTION_MESSAGE));*/

			} catch (RepositoryException repositoryEx) {
				/*log.info("Error:" + repositoryEx.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));*/

			} catch (Exception ex) {

				/*log.info("Error:" + ex.getLocalizedMessage());
				errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
						FetchErrorMessage.getErrorMessage(ErrorCodeConstants.SYSTEM_UNAVAILABLE_MESSAGE));*/

			}
		return entitySpecification;

	
	 
	}
	

	
}
