package uk.co.three.rebus.three_rebus.core.servlets;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import uk.co.three.rebus.three_rebus.core.productmodel.constants.CatalogReadConstants;
import uk.co.three.rebus.three_rebus.core.productmodel.plain.ProductDetailsProvider;
import uk.co.three.rebus.three_rebus.core.productmodel.request.GetProductDetailsRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.GetProductDetailsResponse;

@SlingServlet(paths = "/bin/productCatalog")
public class GetProductDetails extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		ProductDetailsProvider productDetailsProvider = new ProductDetailsProvider();

		String productPath = request
				.getParameter(CatalogReadConstants.PRODUCT_DETAIL_PATH);

		GetProductDetailsRequest getProductDetailsRequest = new GetProductDetailsRequest();
		getProductDetailsRequest.setProductPath(productPath);

		GetProductDetailsResponse productDetails = productDetailsProvider
				.getProductDetail(getProductDetailsRequest);
		
		// 
	}

}
