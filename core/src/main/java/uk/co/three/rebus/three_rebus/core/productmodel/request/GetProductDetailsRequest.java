/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.request;

import uk.co.three.rebus.three_rebus.core.request.BaseRequest;


/**
 * This is the request class for getting all the products
 * This class does not have any data members at this point
 *  
 * @author DD00493288
 *
 */
public class GetProductDetailsRequest extends BaseRequest {

	/*private Session session = null;*/
	
	private String productPath = null;

	/**
	 * @return the productPath
	 */
	public String getProductPath() {
		return productPath;
	}

	/**
	 * @param productPath the productPath to set
	 */
	public void setProductPath(String productPath) {
		this.productPath = productPath;
	}

	/**
	 * @return the session
	 *//*
	public Session getSession() {
		return session;
	}

	*//**
	 * @param session the session to set
	 *//*
	public void setSession(Session session) {
		this.session = session;
	}*/
	
}
