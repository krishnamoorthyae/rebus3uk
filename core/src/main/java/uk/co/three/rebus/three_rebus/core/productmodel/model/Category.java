/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.productmodel.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * This is the model class for representing category data to be used in AEM
 * services This class will return the AEM category node properties For more
 * detailed information about category, caller should use getEPCCategoryData()
 * method
 * 
 * 
 *
 */
public class Category implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Contained category object - source of category data
	 */
	private epc.h3g.com.Category category = null;

	/**
	 * Constructor of the class
	 * 
	 * @param category
	 */
	public Category(epc.h3g.com.Category category) {
		this.category = category;
	}

	/**
	 * This method should be used when we need the EPC category data Caller can
	 * then get all the data contained in the EPC category
	 * 
	 * @return
	 */
	public epc.h3g.com.Category getEPCCategoryData() {
		return category;
	}

	/**
	 * gets the name
	 * 
	 * @return
	 */
	public String getName() {
		return category.getName();
	}

	/**
	 * gets the catalog id
	 * 
	 * @return
	 */
	public String getCatalogId() {
		return category.getCatalogId();
	}

	/**
	 * gets the id value
	 * 
	 * @return
	 */
	public Long getId() {
		return category.getId();
	}

	/**
	 * 
	 * @return
	 */
	public Date getValidFrom() {
		XMLGregorianCalendar validFrom = category.getValidFrom();
		/**
		 * TODO complete the valid date
		 */
		Date validDate = new Date();
		return validDate;
	}

}
