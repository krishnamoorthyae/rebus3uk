package uk.co.three.rebus.three_rebus.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.version.Version;
import javax.jcr.version.VersionHistory;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.three.rebus.three_rebus.core.commons.ErrorCodeConstants;
import uk.co.three.rebus.three_rebus.core.commons.FetchErrorMessage;
import uk.co.three.rebus.three_rebus.core.productmodel.request.ProductDataGetRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.request.ProductOfferRequest;
import uk.co.three.rebus.three_rebus.core.productmodel.response.ProductDataGetResponse;
import uk.co.three.rebus.three_rebus.core.productmodel.service.OfferingsDetailsProvider;
import uk.co.three.rebus.three_rebus.core.productmodel.service.ProductImporterService;
import uk.co.three.rebus.three_rebus.core.productmodel.write.CatalogInfomationWriter;
//import uk.co.three.rebus.three_rebus.core.service.QueryProvider;
import uk.co.three.rebus.three_rebus.core.session.SessionHandlerImpl;
import uk.co.three.rebus.three_rebus.core.util.CommonUtils;
import uk.co.three.rebus.three_rebus.core.util.RebusUtils;

/**
 * This class is responsible for providing utilite's to handle product catalog
 * in repository .
 * 
 * @author MS00506946
 *
 */

@Component
@Service(value = { ProductImporterService.class })
public class ThreeProductImporter2 extends SessionHandlerImpl implements ProductImporterService {

	

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	private OfferingsDetailsProvider offeringsDetailsProvider;

	@Reference
	//private QueryProvider queryProviderImp;
	protected String ABSOLUTE_PATH = "/content/three_rebus/products";
	//7/6
	protected String STORE_NAME = "RebusStoreNew";
	protected String VERSION_NAME = "Version1";
	//protected String latestVersion = "Version1";
	protected String CATALOGUE_NAME = "CatalogOne";
	private Map<Integer, String> errorMap = new HashMap<Integer, String>();

	//8/6
	
	@Override
	@Activate
	public void doImport() {
		getProductDataResponse();
	}

	@Override
	//@Activate
	public ProductDataGetResponse getProductDataResponse() {
		Session session = null;
		String productPath=null ;
		ProductDataGetResponse productDataGetResponse = new ProductDataGetResponse();
		try {
			/**
			 * Code commented to fix closed session issue
			 */
			/*ResourceResolver resourceResolver = getResourceResolver();
			session = openSession();*/
			/**
			 * New code to get the session
			 */
			session = getSession();
			Node storeRoot = session.getNode(ABSOLUTE_PATH);
			
			/**
			 * Code commented to fix closed session issue
			 */
			/*Resource resource = resourceResolver.getResource(ABSOLUTE_PATH);*/
			/*Node storeRoot = resource.adaptTo(Node.class);*/
			Node catalogNode;
			Node storeNode;
			if (!storeRoot.hasNode(STORE_NAME)) {
				storeNode=addStoreName(session, ABSOLUTE_PATH);
				//String storePath=storeNode.getPath();
				
			} else {
				//7/6
					storeNode=storeRoot.getNode(STORE_NAME);
			}
				//String storePath=storeNode.getPath();
				//String storePath=ABSOLUTE_PATH + "/" + STORE_NAME;
				Node versionNode=compareVersionNode(storeNode, session);
				//productPath =ABSOLUTE_PATH + "/" + STORE_NAME+"/"+CATALOGUE_NAME;
				catalogNode=versionNode.addNode(CATALOGUE_NAME);
				catalogNode.setProperty(RebusUtils.TYPE, RebusUtils.CATALOG);
				productPath = versionNode.getPath()+"/"+CATALOGUE_NAME;
				addProduct(session, productPath);
			
			CommonUtils.setErrorMessages(productDataGetResponse, errorMap);
		} catch (LoginException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.LOGIN_EXEPTION,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.LOGIN_EXEPTION_MESSAGE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			productDataGetResponse.addErrorObject(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
		return productDataGetResponse;
	}

	/**
	 * This method will add store name into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,absolutePath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param absolutePath
	 *            shows exact location where operation need to perform .
	 * 
	 * @return void
	 * 
	 */

	private Node addStoreName(Session session, String absolutePath) {
		Node storeNode=null;
		try {
			Node productsNode = session.getNode(absolutePath);
			storeNode = productsNode.addNode(STORE_NAME);
			//7/6
			storeNode.setProperty(RebusUtils.TYPE, RebusUtils.STORE);
			
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
		return storeNode;
	}

	private Node addVersion(Session session, String path,int versionNumber) {
		Node versionNode = null;
		try {
		Node storeNode = session.getNode(path);
		String versionName="Version"+versionNumber;
		 versionNode = storeNode.addNode(versionName);
		 versionNode.setProperty(RebusUtils.VERSION, versionNumber);
		
	} catch (RepositoryException e) {
		log.info("Error:" + e.getLocalizedMessage());
		log.info("Error:" + e.getLocalizedMessage());
		errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
				FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
	}
		return versionNode;
	
	}
	private Node compareVersionNode(Node storeNode,Session session)
	{
		Node versionNode = null;
		try {
			String path=storeNode.getPath();
			NodeIterator versionNodeList= storeNode.getNodes();
			versionNodeList.getSize();
			List<String> versionList=new ArrayList<String>();
			String version = null;
				while(versionNodeList.hasNext())
				{
					versionNode=versionNodeList.nextNode();
					version=versionNode.getProperty(RebusUtils.VERSION).toString();
					versionList.add(version);
				}
				versionList.size();
				int versionNumber=Integer.parseInt(version);
				versionNode=addVersion(session, path,(versionNumber+1));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(ErrorCodeConstants.REPOSITORY_FAILURE_MESSAGE));
		}
		return versionNode;
		
	}
	/**
	 * This method will add product into repository(catalogOne) ,This will
	 * create node.
	 * 
	 * Passing session ,productPath
	 * 
	 * @param Session
	 *            object to handle Transactions performed on repository .
	 * @param productPath
	 *            shows exact location where product need to be stored .
	 * 
	 * @return void
	 * 
	 */

	private void addProduct(Session session, String productPath) {
		try {
			CatalogInfomationWriter catalogInfomationWriter = new CatalogInfomationWriter();
			Node catalogNode = session.getNode(productPath);
			ProductDataGetRequest productDataGetRequest = new ProductDataGetRequest();
			productDataGetRequest.setSession(session);
			ProductDataGetResponse productDataGetResponse = offeringsDetailsProvider
					.getOfferingsDetails(productDataGetRequest);
			ProductOfferRequest productOfferRequest = new ProductOfferRequest();
			if (!productDataGetResponse.hasErrors()) {
				productOfferRequest.setCatalogNode(catalogNode);
				productOfferRequest.setProductDataGetResponse(productDataGetResponse);
				productOfferRequest.setSession(session);
				productDataGetResponse = catalogInfomationWriter.offerDetailsProcessor(productOfferRequest);
				session.save();
			}
		} catch (PathNotFoundException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.PATHNOTFOUND_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.PATHNOTFOUND_FAILURE)));
		} catch (RepositoryException e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.REPOSITORY_FAILURE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.REPOSITORY_FAILURE)));
		} catch (Exception e) {
			log.info("Error:" + e.getLocalizedMessage());
			errorMap.put(ErrorCodeConstants.SYSTEM_UNAVAILABLE,
					FetchErrorMessage.getErrorMessage(String.valueOf(ErrorCodeConstants.SYSTEM_UNAVAILABLE)));
		}
	}
}
