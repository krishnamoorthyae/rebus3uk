//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 05:11:29 PM IST 
//


package epc.h3g.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>3rd party related price specifications. Specialization of price plan specification. Allows to define separate price plans for retailers and MVNOs. Such price plans can be related to a set of party roles (default pricing), or to a set of specific parties. Price plan specified here is for a specific party roles, or for specific parties. Also allows backwards reference of a set of product offerings, e.g., a price plan that covers tariff plan and device in one.</p>
 * 
 * <p>Java class for PartyPricePlanSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyPricePlanSpecification">
 *   &lt;complexContent>
 *     &lt;extension base="{http://com.h3g.epc/}PricePlanSpecification">
 *       &lt;sequence>
 *         &lt;element name="party" type="{http://com.h3g.epc/}Party" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partyRole" type="{http://com.h3g.epc/}PartyRole" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productOffering" type="{http://com.h3g.epc/}ProductOfferingSpecification" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyPricePlanSpecification", propOrder = {
    "party",
    "partyRole",
    "productOffering"
})
public class PartyPricePlanSpecification
    extends PricePlanSpecification
{

    protected List<Party> party;
    protected List<PartyRole> partyRole;
    protected List<ProductOfferingSpecification> productOffering;

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Party }
     * 
     * 
     */
    public List<Party> getParty() {
        if (party == null) {
            party = new ArrayList<Party>();
        }
        return this.party;
    }

    /**
     * Gets the value of the partyRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRole }
     * 
     * 
     */
    public List<PartyRole> getPartyRole() {
        if (partyRole == null) {
            partyRole = new ArrayList<PartyRole>();
        }
        return this.partyRole;
    }

    /**
     * Gets the value of the productOffering property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productOffering property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductOffering().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductOfferingSpecification }
     * 
     * 
     */
    public List<ProductOfferingSpecification> getProductOffering() {
        if (productOffering == null) {
            productOffering = new ArrayList<ProductOfferingSpecification>();
        }
        return this.productOffering;
    }

}
