//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 05:11:29 PM IST 
//


package epc.h3g.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Resource facing service specialization of the service specification. Resource facing services (RFS) are technical and are not visible to the customer, i.e., customer is not aware of them. They are OSS related.</p>
 * 
 * <p>Java class for ResourceFacingServiceSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResourceFacingServiceSpecification">
 *   &lt;complexContent>
 *     &lt;extension base="{http://com.h3g.epc/}ServiceSpecification">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResourceFacingServiceSpecification")
public class ResourceFacingServiceSpecification
    extends ServiceSpecification
{


}
