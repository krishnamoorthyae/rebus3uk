//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 05:11:29 PM IST 
//


package epc.h3g.com;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductOfferingPricePlanContributionTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProductOfferingPricePlanContributionTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Parent"/>
 *     &lt;enumeration value="Child"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProductOfferingPricePlanContributionTypes")
@XmlEnum
public enum ProductOfferingPricePlanContributionTypes {


    /**
     * <p>Defines PSR structure</p>
     * 
     */
    @XmlEnumValue("Parent")
    PARENT("Parent"),

    /**
     * <p>Relationship between complex and simple product offerings, includes additional <i>ProductOfferingRelationshipSpecification</i> data.</p>
     * 
     */
    @XmlEnumValue("Child")
    CHILD("Child");
    private final String value;

    ProductOfferingPricePlanContributionTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProductOfferingPricePlanContributionTypes fromValue(String v) {
        for (ProductOfferingPricePlanContributionTypes c: ProductOfferingPricePlanContributionTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
