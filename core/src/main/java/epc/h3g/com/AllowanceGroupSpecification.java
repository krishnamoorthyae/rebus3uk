//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 05:11:29 PM IST 
//


package epc.h3g.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Allowance group specification on the price plan level. Price plan comprises a list of allowance group specifications. Allowance group specification groups a set of allowance price item specifications that can be used in the "build your own" configuration. Build your own configuration allows end customer to build his own composition of allowances that fit a designated financial limit. For example, customer can build his combination of allowances to fit (for example) 30 GBP.</p>
 * 
 * <p>Java class for AllowanceGroupSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllowanceGroupSpecification">
 *   &lt;complexContent>
 *     &lt;extension base="{http://com.h3g.epc/}EntitySpecification">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllowanceGroupSpecification")
public class AllowanceGroupSpecification
    extends EntitySpecification
{


}
