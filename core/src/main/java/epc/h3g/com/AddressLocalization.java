//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 05:11:29 PM IST 
//


package epc.h3g.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Address localization data, used to localize parts of the address, such as state or province name, city name, or street name. Some parts of the World have different multilingual names. For example Bruxelles (fr_be) / Brussels (nl_be), or Rijeka (hr_hr) / Fiume (it_it).</p>
 * 
 * <p>Java class for AddressLocalization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressLocalization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="localizedCityName" type="{http://com.h3g.epc/}Localization" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="localizedStateOrProvinceName" type="{http://com.h3g.epc/}Localization" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="localizedStreetName" type="{http://com.h3g.epc/}Localization" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressLocalization", propOrder = {
    "localizedCityName",
    "localizedStateOrProvinceName",
    "localizedStreetName"
})
public class AddressLocalization {

    protected List<Localization> localizedCityName;
    protected List<Localization> localizedStateOrProvinceName;
    protected List<Localization> localizedStreetName;

    /**
     * Gets the value of the localizedCityName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the localizedCityName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocalizedCityName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Localization }
     * 
     * 
     */
    public List<Localization> getLocalizedCityName() {
        if (localizedCityName == null) {
            localizedCityName = new ArrayList<Localization>();
        }
        return this.localizedCityName;
    }

    /**
     * Gets the value of the localizedStateOrProvinceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the localizedStateOrProvinceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocalizedStateOrProvinceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Localization }
     * 
     * 
     */
    public List<Localization> getLocalizedStateOrProvinceName() {
        if (localizedStateOrProvinceName == null) {
            localizedStateOrProvinceName = new ArrayList<Localization>();
        }
        return this.localizedStateOrProvinceName;
    }

    /**
     * Gets the value of the localizedStreetName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the localizedStreetName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocalizedStreetName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Localization }
     * 
     * 
     */
    public List<Localization> getLocalizedStreetName() {
        if (localizedStreetName == null) {
            localizedStreetName = new ArrayList<Localization>();
        }
        return this.localizedStreetName;
    }

}
