
$(document).ready(function(){
	$("body").on("click",".btn-clipboard",function(){
		var $this = $(this);
		$this.html("copied..");
		
		setTimeout(function() {
			$this.html("Copy Code");
		}, 800); 
		copytoClipboard($(this).attr("rel"));
	});
	
	$("body").on("click",".btn-clipboard-path",function(){
		var $this = $(this);
		$this.html("copied..");
		
		setTimeout(function() {
			$this.html("Copy Path");
		}, 800); 
		copypathtoClipboard($(this).attr("rel"));
	});
	
	$("body").on("click",".goto-search",function(){
		$(".section-search").addClass("afterclick");
	});
	
	$("body").on("click",".close-search",function(){
		$(".section-search").addClass("op1").removeClass("afterclick");
		setTimeout(function() {
			$(".section-search").addClass("op2").removeClass("op1")
		}, 400);
		setTimeout(function() {
			$(".section-search").removeClass("op2")
		}, 400); 
	});
	$("body").on("click",".wishlist-icon",function(){
		$(this).prev().addClass("h-14");
		setTimeout(function() {
			$(".item-wish").removeClass("h-14")
		}, 2000); 
	});
	
	/*MegaMenu*/
	$("body").on("mouseenter",".mega-dropdown",function() {
			$(this).find('.mega-dropdown-menu').addClass("show-mega-menu");

			//$(this).toggleClass('hidden');        
		}).on("mouseleave",".mega-dropdown",function() {
			$(this).find('.mega-dropdown-menu').removeClass("show-mega-menu");
			//$(this).toggleClass('hidden');       
		}
	);
	$("body").on("click",".filter-check",function() {
		var deviceClass = "device"+randomNo();
		var chkState = $(this).is(":checked");
		var selectedBoxlen = $(this).closest(".mega-dropdown-menu").find("input[type='checkbox']:checked").length;
		var filterName = $(this).closest(".mega-dropdown-menu").prev("a.dropdown-toggle").find(".filter-label").html();
		if(chkState && selectedBoxlen > 0){
			$(this).closest(".mega-dropdown-menu").prev().find(".tick-brand").removeClass("hidden");
			/*js for tags*/
			var selectedBoxlabel = $(this).next(".check-position").html();
			$(this).addClass(deviceClass).attr("data-rel",deviceClass);
			var tagObj = {
				"tagname":selectedBoxlabel,
				"tagclass":deviceClass,
			}
			gettagHTML = taghtml(tagObj)
			
			var filterId = filterName.replace(" ",'');
			var $filterDiv = $('#'+filterId);
			
			if($filterDiv && ($filterDiv.length>0)){
				$("#"+filterId).append(gettagHTML);
				
			}else{
				filterHtml = '<div id="'+filterId+'"><label class="filterName col-xs-12">'+filterName+'</label></div>';
				$(".filter-list").append(filterHtml);
				$("#"+filterId).append(gettagHTML);
			}
			
		}else{
			var tagclass = $(this).attr("data-rel");
			$(".filter-list ."+tagclass).remove();
			if(selectedBoxlen == 0){
				$(this).closest(".mega-dropdown-menu").prev().find(".tick-brand").addClass("hidden")
			}
			
			
		}
	});
	$("body").on("click",".removetag",function() {
		getinputClass = $(this).attr("rel");
		$(this).parent(".tag").remove();
		var taglen = $(this).parent().parent().find("span.tag").length;
		if(taglen == 0){
			$(this).parent().parent().remove();
		}
		$("input."+getinputClass).trigger("click");
	});
	$("body").on("click",".txt-filter",function() {
									$(".js-navbar-collapse").toggleClass("hidden-xs hidden-sm");
									setTimeout(function() {
			$(".nav5").toggleClass("w-full");
		}, 100); 
									
								});
	$("body").on("click",".button-section .btn", function(){
		if($(this).closest(".help-quest").next(".help-quest").hasClass("hidden")){
			$(".help-quest").addClass("hidden");
			$(this).closest(".help-quest").next(".help-quest").removeClass("hidden")
		}else{
			$(".help-quest-end").removeClass("hidden");
			$(".help-quest").addClass("hidden");
		}
	})							

	/*Accordion*/
	 $('body').on("click",".accordion-toggle",function(){
		$(".tech-specifications .glyphicon-minus").toggleClass('glyphicon-minus glyphicon-plus');
	  $(this).next(".glyphicon").toggleClass('glyphicon-plus glyphicon-minus');

	  });	
	  
	$("body").on("click",".chkbtn",function(event){
		var count = parseInt($(this).attr("data"));
		$(".bootstrapWizard li").eq(count).find("a").trigger("click");
		$(window).scrollTop($('#checkout-breadcrumb').offset().top);
		event.preventDefault();
	
		
	})
	  
	 
});//Document Ready END
function copytoClipboard(element){
	  var $temp = $("<textarea>");
	  $("body").append($temp);
	  $temp.val($.trim($(element).html())).select();
	  document.execCommand("copy");
	  $temp.remove();
}
function copypathtoClipboard(element){
	  var $temp = $("<textarea>");
	  $("body").append($temp);
	  var htmltxt = "<div include-html='"+$(element).attr("rel")+"'></div>"
	  $temp.val($.trim(htmltxt)).select();
	  document.execCommand("copy");
	  $temp.remove();
}

function includeHTML(){
	var checkLen = $(".includeFile").length;
	$.each($(".includeFile"), function(key,val){
	var $this = $(this);	
	var pageURL = $this.attr("include-html");	
		$.ajax({
			url: pageURL,
			success: function(result){
				$(".includeFile").eq(key).html(result);
			}});
			//includeHTML();
		
	})
	
}
function randomNo(){
	return Math.floor((Math.random() * 1000) + 1);
	
}
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
function taghtml(obj){
	return '<span class="tag label label-info '+obj.tagclass+'">'+obj.tagname+'<span class="removetag" rel="'+obj.tagclass+'">X</span></span>';
}