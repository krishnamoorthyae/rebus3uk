
$(document).ready(function(){
	$(".btn-clipboard").click(function(element){
		var $this = $(this);
		$this.html("copied..");
		
		setTimeout(function() {
			$this.html("copy");
		}, 800); 
		copytoClipboard($(this).attr("rel"));
	});
	
	$(".goto-search").click(function(){
		$(".section-search").addClass("afterclick");
	});
	$(".close-search").click(function(){
		$(".section-search").addClass("op1").removeClass("afterclick");
		setTimeout(function() {
			$(".section-search").addClass("op2").removeClass("op1")
		}, 400);
		setTimeout(function() {
			$(".section-search").removeClass("op2")
		}, 400); 
	});
	$(".wishlist-icon").click(function(){
		$(this).prev().addClass("h-14");
		setTimeout(function() {
			$(".item-wish").removeClass("h-14")
		}, 2000); 
	});
})
function randomNo(){
	return Math.floor((Math.random() * 1000) + 1);
	
}
function copytoClipboard(element){
	  var $temp = $("<textarea>");
	  $("body").append($temp);
	  $temp.val($.trim($(element).html())).select();
	  document.execCommand("copy");
	  $temp.remove();
}
function includeHTML() {
	var z,
	i,
	a,
	file,
	xhttp;
	z = document.getElementsByTagName("*");
	for (i = 0; i < z.length; i++) {
		if (z[i].getAttribute("include-html")) {
			a = z[i].cloneNode(false);
			file = z[i].getAttribute("include-html");
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					a.removeAttribute("include-html");
					a.innerHTML = xhttp.responseText;
					z[i].parentNode.replaceChild(a, z[i]);
					includeHTML();
				}
			}
			xhttp.open("GET", file, true);
			xhttp.send();
			return;
		}
	}
}