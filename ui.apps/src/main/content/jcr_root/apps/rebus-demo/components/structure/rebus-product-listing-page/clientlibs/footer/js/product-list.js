 $(document).ready(function() {
									var $compCheckBox = $(".check-box input[type='checkbox");
							
								$('body').on('click', ".check-box input[type='checkbox']", function() {
                                    var checkstate = $(this).is(":checked");
									
                                    var len = $(".check-box input[type='checkbox']:checked").length;
									
                                    if (checkstate && len < 4) {
									$(this).prop("checked",true);
									   $(".section-bottom-icons").removeClass("hidden");	
                                      var deviceName = $(this).attr("data");
                                      deviceName = deviceName.replace(/"/g, "");
                                      var deviceImg = $(this).attr("data-rel");
									  var deviceClass = "device"+randomNo();
									  $(this).attr("data-no", deviceClass).addClass(deviceClass);
									  $(this).parent().addClass("hidden");
									  var removeHTML = "<div class='resetcheck "+deviceClass+"' rel='"+deviceClass+"'><span>Remove</span></div>";
									  $(this).parent().after(removeHTML);
									  setTimeout(function() {
											 $(".resetcheck."+deviceClass+" span").addClass("h-25");
											 $(".image-bord."+deviceClass).addClass("w-25p");
										}, 50); 
									 
                                      var obj = {
                                        "deviceImg": deviceImg,
                                        "deviceName": deviceName,
										"deviceClass": deviceClass
                                      }
                                      var compareHTML = compareBox(obj);
                                      $(compareHTML).insertBefore(".comparebtn");
									  
                                      if (len == 3) {
										
										$compCheckBox.prop("disabled",true);
										$(".check-box span").addClass("txt-cross");
                                      }
                                    }else{
									
										var deviceNo = $(this).attr("data-no");
										$(".image-bord."+deviceNo).remove();
										$(this).attr("data-no","").removeClass(deviceNo);
										$compCheckBox.prop("disabled",false);
										//$(this).prop("checked",false);
									   $(".check-box span").removeClass("txt-cross");
									  
									  
										
									}
									
                                  })
								  
								  $("body").on("click",".close-ico", function(){
									var deviceNo = $(this).attr("rel");
									$("input[type='checkbox']."+deviceNo).parent().removeClass("hidden")
									$("input[type='checkbox']."+deviceNo).prop("checked",false).attr("data-no","").removeClass(deviceNo);
									$compCheckBox.prop("disabled",false);
									   $(".check-box span").removeClass("txt-cross");
									$(".image-bord."+deviceNo).remove();
									$(".resetcheck."+deviceNo).remove();
									 isoLen = $(".check-box input[type='checkbox']:checked").length;
									 if(isoLen == 0){
									    $(".section-bottom-icons").addClass("hidden");
									   }
									
										
								  });
								  $("body").on("click",".resetcheck", function(){
									   var deviceNo = $(this).attr("rel");
										$("input[type='checkbox']."+deviceNo).parent().removeClass("hidden")
										$("input[type='checkbox']."+deviceNo).prop("checked",false).attr("data-no","").removeClass(deviceNo);
									   $(".check-box input[type='checkbox").prop("disabled",false);
									   $(".check-box span").removeClass("txt-cross");
									  
									   $(".image-bord."+deviceNo).remove();
									   $(this).remove();
									  selectedBoxCount = $(".check-box input[type='checkbox']:checked").length; 
									 if(selectedBoxCount == 0){
									    $(".section-bottom-icons").addClass("hidden");
									   }
                                });
								$("body").on("click",".not-now",function(){
									$(".help").css({"opacity":"0"});
									setTimeout(function() {
										$(".help").hide();
									}, 800); 
								})
                                });

                                function compareBox(obj) {
                                  var htmltxt = '<div class="col-xs-12 col-sm-6 col-md-3 image-bord '+obj.deviceClass+' ">';
                                  htmltxt += '<img src="' + obj.deviceImg + '" alt="' + obj.deviceImg + '" class="compare-device-img">';
                                  htmltxt += '<span class="phone-icon">' + obj.deviceName + '</span>';
                                  htmltxt += '<span class="close-ico" rel="'+obj.deviceClass+'">X</span>';
                                  htmltxt += '</div>';
                                  return htmltxt;
                                }